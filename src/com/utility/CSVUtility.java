package com.utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.csvreader.CsvReader;

public class CSVUtility {
	public static List<Map<String, Map<String, String>>> getCsvElements(String fileName) {
		List<Map<String, Map<String, String>>> list = null;
		File file = null;
		FileInputStream fileInputStream = null;
		CsvReader csvReader = null;
		try {
			list = new ArrayList<Map<String, Map<String, String>>>();
			file = new File(fileName);
			fileInputStream = new FileInputStream(file);
			csvReader = new CsvReader(fileInputStream, ',',Charset.forName("UTF-8"));
			csvReader.setSafetySwitch(false);
			String[] headers = null;
			if (false != csvReader.readHeaders()) {
				headers = csvReader.getHeaders();
			}
			String[] record = null;
			while (false != csvReader.readRecord()) {
				record = csvReader.getValues();
				Map<String, Map<String, String>> map = new HashMap<String, Map<String, String>>();
				Map<String, String> tempMap = new HashMap<String, String>();
				for (int j = 1; j < record.length; j++) {
					if (null != headers && j < headers.length && !headers[j].isEmpty()) {
						tempMap.put(headers[j], record[j]);
					}
				}
				map.put(record[0], tempMap);
				list.add(map);
			}
			csvReader.close();
			fileInputStream.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				if (null != csvReader) {
					csvReader.close();
					fileInputStream.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return list;
	}
}
