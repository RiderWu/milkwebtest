package com.testcase;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.dataprovider.TestDataProvider;


import com.pageobject.HomePage;
import com.pageobject.MilkNutritionArticlesPage;
import com.web.Common;

public class MilkNutritionArticles_Test {

	public static String StartTime = null;
	public WebDriver driver = null;
	public static String Domain = null;
	public static String Browser = null;
	public static String Market = null;
	public static String Width = null;
	public static String Height = null;
	private static String Path = "/milk-nutrition-articles";

	@Parameters({ "environment" })
	@BeforeSuite
	public void beforeSuite(@Optional("https://milklife.com")String environment) throws Exception {
		SimpleDateFormat datetime = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		StartTime = datetime.format(new Date());
		Domain = environment;
	}

	@AfterSuite
	public void afterSuite() throws Exception {
		// MailUtility.sendMail();
	}

	@Parameters({ "browser", "market", "width", "height" })
	@BeforeClass
	public void beforeClass(@Optional("Chrome")String browser, @Optional("en")String market, @Optional("1366")String width, @Optional("768")String height) throws Exception {
		Browser = browser;
		Market = market;
		Width = width;
		Height = height;
		driver = Common.openBrowser(Browser, "about:blank", width, height);
		
	}

	@AfterClass
	public void afterClass() throws Exception {
		driver.quit();
		
	}

	@BeforeMethod
	public void beforeMethod() throws Exception {

	}

	@AfterMethod
	public void afterMethod() throws Exception {
		
	}

	@Test
	public void Test_MilkNutritionArticlesPageLoad() throws Exception {
		try {
			
			driver.navigate().to(Domain + Path);
			Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-MilkNutritionArticlesPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 120000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test()
	public void Test_MilkNutritionArticlesHeroButton() throws Exception {
		try {
			MilkNutritionArticlesPage nutrition = new MilkNutritionArticlesPage(driver);
			driver.navigate().to(Domain + Path);
			
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-HeroButton");
			
			nutrition.getHeroButton().click();
			Thread.sleep(5000);
			Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-ClickHeroButton");
			
			driver.navigate().back();
			
			Assert.assertTrue(nutrition.getHeroButton().isEnabled(), "HeroButton is not enabled");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test()
	public void Test_MilkNutritionArticlesTalkButton() throws Exception {
		try {
			MilkNutritionArticlesPage nutrition = new MilkNutritionArticlesPage(driver);
			driver.navigate().to(Domain + Path);
			
			Common.scrollTo(nutrition.getHeroButton());
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-TalkButton");
			
			nutrition.getTalkButton().click();
			Thread.sleep(7000);
			driver.switchTo().window((String) driver.getWindowHandles().toArray()[driver.getWindowHandles().size()-1]);//切换新页面
			Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-ClickTalkButton");
			
			driver.close();
			driver.switchTo().window((String) driver.getWindowHandles().toArray()[0]);
			
			Assert.assertTrue(nutrition.getHeroButton().isEnabled(), "TalkButton is not enabled");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	@Test()
	public void Test_MilkNutritionArticlesWhyButton() throws Exception {
		try {
			MilkNutritionArticlesPage nutrition = new MilkNutritionArticlesPage(driver);
			driver.navigate().to(Domain + Path);
			
			Common.scrollTo(nutrition.getHeroButton());
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-WhyButton");
			
			nutrition.getWhyButton().click();
			Thread.sleep(5000);
			//driver.switchTo().window((String) driver.getWindowHandles().toArray()[driver.getWindowHandles().size()-1]);//切换新页面
			Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-ClickWhyButton");
			
			driver.navigate().back();
			//driver.switchTo().window((String) driver.getWindowHandles().toArray()[0]);
			
			Assert.assertTrue(nutrition.getWhyButton().isEnabled(), "WhyButton is not enabled");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	@Test()
	public void Test_MilkNutritionArticlesWhatButton() throws Exception {
		try {
			MilkNutritionArticlesPage nutrition = new MilkNutritionArticlesPage(driver);
			driver.navigate().to(Domain + Path);
			
			Common.scrollTo(nutrition.getHeroButton());
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-WhatButton");
			
			nutrition.getWhatButton().click();
			Thread.sleep(5000);
			//driver.switchTo().window((String) driver.getWindowHandles().toArray()[driver.getWindowHandles().size()-1]);//切换新页面
			Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-ClickWhatButton");
			
			driver.navigate().back();
			//driver.switchTo().window((String) driver.getWindowHandles().toArray()[0]);
			
			Assert.assertTrue(nutrition.getWhatButton().isEnabled(), "WhatButton is not enabled");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	@Test
	public void Test_MilkNutritionArticlesNutrition() throws Exception {
		try {
			MilkNutritionArticlesPage nutrition = new MilkNutritionArticlesPage(driver);
			driver.navigate().to(Domain + Path);
			
			
		    Common.scrollTo(nutrition.getNutrition());
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Nutrition");
			
			nutrition.getMostViewed().click();
			Common.scrollTo(nutrition.getNutrition());
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-ClickMostVeviewed");
			
			nutrition.getMostRecent().click();
			Common.scrollTo(nutrition.getNutrition());
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"03-ClickMostRecent");
			
			String recipename = null;
			for(int i=0;i<6;i++){
				recipename = nutrition.getRecipes().get(i).getText().replace("?", "");
				nutrition.getRecipes().get(i).click();
				Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"04-Click "+recipename);
				driver.navigate().back();
			}
			
			Common.scrollTo(nutrition.getLoadmore());
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"05-Loadmore");
			
			nutrition.getLoadmore().click();
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"06-ClickLoadmoreLoading");
			
			Thread.sleep(5000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"07-ClickLoadmoreLoaded");
			
			Assert.assertTrue(nutrition.getMostViewed().isEnabled(), "MostReviewed is not enabled");
			Assert.assertTrue(nutrition.getMostRecent().isEnabled(), "MostRecent is not enabled");
			Assert.assertTrue(nutrition.getLoadmore().isEnabled(), "Loadmore is not enabled");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	@Test()
	public void Test_MilkNutritionArticlesAnyButton() throws Exception {
		try {
			MilkNutritionArticlesPage nutrition = new MilkNutritionArticlesPage(driver);
			driver.navigate().to(Domain + Path);
			
			Common.scrollTo(nutrition.getNutrition());
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Any");
			
			nutrition.getFilter().click();
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-ClickAnyButton");
			nutrition.getItems().get(0).click();
			Thread.sleep(5000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"03-ClickedAnyButton");
			
			
			nutrition.getFilter().click();
			Thread.sleep(1000);
			nutrition.getItems().get(1).click();
			Thread.sleep(5000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"04-ClickBreakfast");
			
			nutrition.getFilter().click();
			Thread.sleep(1000);
			nutrition.getItems().get(2).click();
			Thread.sleep(7000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"05-ClickInfographics");
			
			//driver.navigate().back();
			
			Assert.assertTrue(nutrition.getFilter().isEnabled(), "Any is not enabled");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	
	//ToDo: Please add test cases for MilkNutritionArticlesPage
	
}
