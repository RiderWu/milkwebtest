package com.testcase;

import java.text.SimpleDateFormat;
import java.util.Date;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.pageobject.FunBreakfastIdeasPage;
import com.pageobject.HomePage;
import com.web.Common;

public class FunBreakfastIdeas_Test {

	public static String startTime = null;
	public WebDriver driver = null;
	public static String domain = null;
	public static String browser = null;
	public static String market = null;
	public static String width = null;
	public static String height = null;
	private static String path = "/fun-breakfast-ideas";

	@Parameters({ "environment" })
	@BeforeSuite
	public void beforeSuite(@Optional("https://milklife.com")String environment) throws Exception {
		SimpleDateFormat datetime = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		startTime = datetime.format(new Date());
		domain = environment;
	}

	@AfterSuite
	public void afterSuite() throws Exception {
		// MailUtility.sendMail();
	}

	@Parameters({ "browser", "market", "width", "height" })
	@BeforeClass
	public void beforeClass(@Optional("Chrome")String browser, @Optional("en")String market, @Optional("1366")String width, @Optional("768")String height) throws Exception {
		FunBreakfastIdeas_Test.browser = browser;
		FunBreakfastIdeas_Test.market = market;
		FunBreakfastIdeas_Test.width = width;
		FunBreakfastIdeas_Test.height = height;
		driver = Common.openBrowser(browser, "about:blank", width, height);
		
	}

	@AfterClass
	public void afterClass() throws Exception {
		driver.quit();
		
	}

	@BeforeMethod
	public void beforeMethod() throws Exception {

	}

	@AfterMethod
	public void afterMethod() throws Exception {
		
	}

	@Test
	public void test_FunBreakfastIdeasPageLoad() throws Exception {
		try {
			
			driver.navigate().to(domain + path);
			Common.ScrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-FunBreakfastIdeasPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 120000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	//ToDo: Please add test cases for FunBreakfastIdeasPage

	@Test()
	public void test_ServerItUp() throws Exception {
		try {
			FunBreakfastIdeasPage funBreakfastIdeasPage = new FunBreakfastIdeasPage(driver);
			driver.navigate().to(domain + path);
			
		    Common.scrollTo(funBreakfastIdeasPage.getTextBlock());
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-TextBlock");
			
			funBreakfastIdeasPage.getServeItUp().click();
			Thread.sleep(5000);
			Common.ScrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-ClickServerItUp");
			
			driver.navigate().back();
			
			Assert.assertTrue(funBreakfastIdeasPage.getTextBlock().isEnabled(), "TextBlock is not enabled");
			Assert.assertTrue(funBreakfastIdeasPage.getServeItUp().isEnabled(), "ServeItUp is not enabled");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}

	@Test()
	public void test_ClickProtein() throws Exception {
		try {
			FunBreakfastIdeasPage funBreakfastIdeasPage = new FunBreakfastIdeasPage(driver);
			driver.navigate().to(domain + path);
			
			funBreakfastIdeasPage.getProtein().click();
			Thread.sleep(6000);
			driver.switchTo().window(driver.getWindowHandles().toArray()[driver.getWindowHandles().size()-1].toString());
			Common.ScrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"03-ClickProtein");			
			driver.close();
			driver.switchTo().window(driver.getWindowHandles().toArray()[0].toString());
			Assert.assertTrue(funBreakfastIdeasPage.getProtein().isEnabled(), "Protein is not enabled");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test()
	public void test_WhyBreakfast() throws Exception {
		try {
			FunBreakfastIdeasPage funBreakfastIdeasPage = new FunBreakfastIdeasPage(driver);
			driver.navigate().to(domain + path);		
			funBreakfastIdeasPage.getWhyBreakfast().click();
			Thread.sleep(6000);
			driver.switchTo().window(driver.getWindowHandles().toArray()[driver.getWindowHandles().size()-1].toString());
			Common.ScrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"04-WhyBreakfast");			
			driver.close();
			driver.switchTo().window(driver.getWindowHandles().toArray()[0].toString());
			Assert.assertTrue(funBreakfastIdeasPage.getWhyBreakfast().isEnabled(), "WhyBreakfast is not enabled");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	@Test()
	public void test_Tastethese() throws Exception {
		try {
			FunBreakfastIdeasPage funBreakfastIdeasPage = new FunBreakfastIdeasPage(driver);
			driver.navigate().to(domain + path);		
			funBreakfastIdeasPage.getTastethese().click();
			Thread.sleep(6000);
			Common.ScrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"05-Tastethese");			
			driver.navigate().back();
			Assert.assertTrue(funBreakfastIdeasPage.getTastethese().isEnabled(), "Tastethese is not enabled");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	@Test
	public void test_Breakfast() throws Exception {
		try {
			FunBreakfastIdeasPage funBreakfastIdeasPage = new FunBreakfastIdeasPage(driver);
			driver.navigate().to(domain + path);
			
			Common.scrollTo(funBreakfastIdeasPage.getBreakfast());
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Breakfast");
			
			funBreakfastIdeasPage.getMostViewed().click();
			Common.scrollTo(funBreakfastIdeasPage.getBreakfast());
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-ClickMostReviewed");
			
			funBreakfastIdeasPage.getMostRecent().click();
			Common.scrollTo(funBreakfastIdeasPage.getBreakfast());
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"03-ClickMostRecent");
			
			String recipename = null;
			for(int i=0;i<6;i++){
				recipename = funBreakfastIdeasPage.getRecipes().get(i).getText().replace("?", "");
				funBreakfastIdeasPage.getRecipes().get(i).click();				
				Thread.sleep(1000);
				Common.ScrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"04-Click "+recipename);
				driver.navigate().back();
			}
			
			Common.scrollTo(funBreakfastIdeasPage.getLoadmore());
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"05-Loadmore");
			
			funBreakfastIdeasPage.getLoadmore().click();
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"06-ClickLoadmoreLoading");
			
			Thread.sleep(5000);
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"07-ClickLoadmoreLoaded");
			
			Assert.assertTrue(funBreakfastIdeasPage.getBreakfast().isEnabled(), "WhatsHappening is not enabled");
			Assert.assertTrue(funBreakfastIdeasPage.getMostViewed().isEnabled(), "MostViewed is not enabled");
			Assert.assertTrue(funBreakfastIdeasPage.getMostRecent().isEnabled(), "MostRecent is not enabled");
			Assert.assertTrue(funBreakfastIdeasPage.getLoadmore().isEnabled(), "Loadmore is not enabled");
		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	@Test
	public void test_Any() throws Exception {
		try {
			FunBreakfastIdeasPage funBreakfastIdeasPage = new FunBreakfastIdeasPage(driver);
			driver.navigate().to(domain + path);
			Thread.sleep(3000);
			Common.scrollTo(funBreakfastIdeasPage.getBreakfast());
			Thread.sleep(3000);
			funBreakfastIdeasPage.getAnybutton().click();
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"08-ClickAnyButton");
			funBreakfastIdeasPage.getItems().get(0).click();
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"09-ClickAnyDropdown");
			funBreakfastIdeasPage.getAnybutton().click();
			Thread.sleep(1000);
			funBreakfastIdeasPage.getItems().get(1).click();
			Thread.sleep(5000);
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"10-ClickCookingDropdown");
			funBreakfastIdeasPage.getAnybutton().click();
			Thread.sleep(2000);
			funBreakfastIdeasPage.getItems().get(2).click();
			Thread.sleep(3000);
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"11-ClickGetupDropdown");
			
		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
}
