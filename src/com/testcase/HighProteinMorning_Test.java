package com.testcase;


import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.pageobject.HighProteinMorningPage;
import com.web.Common;

public class HighProteinMorning_Test {

	public static String startTime = null;
	public WebDriver driver = null;
	public static String domain = null;
	public static String browser = null;
	public static String market = null;
	public static String width = null;
	public static String height = null;
	private static String path = "/high-protein-morning";

	@Parameters({ "environment" })
	@BeforeSuite
	public void beforeSuite(@Optional("https://milklife.com") String environment) throws Exception {
		SimpleDateFormat datetime = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		startTime = datetime.format(new Date());
		domain = environment;
	}

	@AfterSuite
	public void afterSuite() throws Exception {
		// MailUtility.sendMail();
	}

	@Parameters({ "browser", "market", "width", "height" })
	@BeforeClass
	public void beforeClass(@Optional("IE") String browser, @Optional("en") String market,
			@Optional("1366") String width, @Optional("768") String height) throws Exception {
		HighProteinMorning_Test.browser = browser;
		HighProteinMorning_Test.market = market;
		HighProteinMorning_Test.width = width;
		HighProteinMorning_Test.height = height;
		driver = Common.openBrowser(HighProteinMorning_Test.browser, "about:blank", width, height);

	}

	@AfterClass
	public void afterClass() throws Exception {
		driver.quit();
	}

	@BeforeMethod
	public void beforeMethod() throws Exception {
	}

	@AfterMethod
	public void afterMethod() throws Exception {
	}

	@Test
	public void HighProteinMorningLoad() throws Exception {
		try {

			driver.navigate().to(domain + path);
			Common.ScrollDownAndTakeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height,
					startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), "01-HighProteinMorningLoad");
			Assert.assertTrue(driver.getPageSource().length() > 120000, "Page content is too short, maybe Error now!");
		} catch (Exception e) {
			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "99-ERROR");
			Assert.assertTrue(false, e.getMessage());

		}
	}
	
	
	@Test()
	public void test_ChevronSymbol() throws Exception {
		try {
		HighProteinMorningPage HighProteinMorning = new HighProteinMorningPage(driver);
	//		driver.navigate().to(domain + path);
			Thread.sleep(3000);
			Common.scrollTo(HighProteinMorning.getTopHeader());
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "01-ChevronSymbol");
			HighProteinMorning.getChevronSymbol().click();
			Thread.sleep(2000);
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "02-ClickChevronSymbol");
		
			Assert.assertTrue(HighProteinMorning.getChevronSymbol().isEnabled(), "ChevronSymbol button is not enabled");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "99-ERROR");
			Assert.assertTrue(false, e.getMessage());

		}
	}
	
	@Test()
	public void test_ProteinBasics() throws Exception {
		try {
			HighProteinMorningPage HighProteinMorning = new HighProteinMorningPage(driver);
	//	 driver.navigate().to(domain + path);
			Thread.sleep(6000);
			Common.scrollTo(HighProteinMorning.getTopHeader());
			HighProteinMorning.getChevronSymbol().click();
			
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "01-Video");
			Thread.sleep(3000);
			HighProteinMorning.getVideoPlay().click();
			Thread.sleep(30000);
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "02-ClickPlayVideo");
		//	Common.scrollTo(HighProteinMorning.getGreyNavigation());
			HighProteinMorning.getVideoClose().click();
			Thread.sleep(2000);
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "03-ClickCloseVideo");	
			
			Common.scrollTo(HighProteinMorning.getCopy1());
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "04-TheImportanceOfHighProteinFoods");
			
			for(int i=0;i<3;i++){
				HighProteinMorning.getProteinTabItems1().get(i).click();
				Thread.sleep(3000);
				Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "05-TheImportanceOfHighProteinFoodsClick" + i);
				HighProteinMorning.getProteinTabItems1().get(i).click();
				Thread.sleep(2000);
		}
			
			Assert.assertTrue(HighProteinMorning.getVideoPlay().isEnabled(), "video play button is not enabled");
			Assert.assertTrue(HighProteinMorning.getVideoClose().isEnabled(), "video close button is not enabled");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "99-ERROR");
			Assert.assertTrue(false, e.getMessage());

		}
	}
	
	
	
	
	
	@Test()
	public void test_ProteinInMilk() throws Exception {
		try {
			HighProteinMorningPage HighProteinMorning = new HighProteinMorningPage(driver);
	//	driver.navigate().to(domain + path);
			Thread.sleep(1000);
			
			Common.scrollTo(HighProteinMorning.getCopy2());
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "01-ProteinInMilk");
			
			for(int i=0;i<3;i++){
			//	Common.scrollTo(HighProteinMorning.getCopy2());
				Thread.sleep(1000);
				HighProteinMorning.getProteinTabItems2().get(i).click();
				Thread.sleep(1000);
				Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
						Thread.currentThread().getStackTrace()[1].getMethodName(), "02-ProteinInMilkClick" + i);
				HighProteinMorning.getProteinTabItems2().get(i).click();
			}
			
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "03-LearnMore");
			Common.scrollTo(HighProteinMorning.getLearnMore());
			HighProteinMorning.getNutrientValue().click();
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "04-ClickNutrientValueButton");
			
			HighProteinMorning.getCost().click();
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "05-ClickCostButton");
			
			HighProteinMorning.getVersatility().click();
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "06-ClickVersatilityButton");
			
			HighProteinMorning.getProteinQuality().click();
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "07-ClickProteinQualityButton");
			
			Assert.assertTrue(!HighProteinMorning.getProteinTabItems2().isEmpty(), "tab buttons in Protein in Milk are empty");
			Assert.assertTrue(HighProteinMorning.getProteinQuality().isEnabled(), "Protein Quality button is not enabled");
			Assert.assertTrue(HighProteinMorning.getNutrientValue().isEnabled(), "getNutrient Value button is not enabled");
			Assert.assertTrue(HighProteinMorning.getCost().isEnabled(), "Cost button is not enabled");
			Assert.assertTrue(HighProteinMorning.getVersatility().isEnabled(), "Versatility button is not enabled");
			

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "99-ERROR");
			Assert.assertTrue(false, e.getMessage());

		}
	}
	

	
	@Test()
	public void test_ProteinIntake() throws Exception {
		try {
			HighProteinMorningPage HighProteinMorning = new HighProteinMorningPage(driver);
	//driver.navigate().to(domain + path);
			Thread.sleep(1000);
			Common.scrollTo(HighProteinMorning.getIntakeTitle());
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "01-HowMuchProteinYouNeed");
		    
			Actions action = new Actions(driver);
			WebElement source1 = driver.findElement(By.xpath("//div[@id='js-rangeslider-0']/div[2]"));
			action.clickAndHold(source1).moveByOffset(50, 0);
			action.moveToElement(source1).release();
			Action actions1 = action.build();
			actions1.perform();
			Thread.sleep(2000);
			
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "02-ClickHandleBtn");
			HighProteinMorning.getAgeBtn().click();
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "03-ClickAgeDropDown");
			HighProteinMorning.getAgeOption().click();
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "04-SelectAge");
			HighProteinMorning.getNotActive().click();
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "05-SelectNotActive");
			//*[@id="js-rangeslider-1"]/div[2]
			WebElement source2 = driver.findElement(By.xpath("//div[@id='js-rangeslider-1']/div[2]"));
			action.clickAndHold(source2).moveByOffset(235, 0);
			action.moveToElement(source2).release();
			Action actions2 = action.build();
			actions2.perform();
			Thread.sleep(2000);
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "06-SelectVeryActive");
			
			Common.scrollTo(HighProteinMorning.getCopy3());
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "07-TimingMattersForAHighProteinBreakfast");
			
			for(int i=0;i<2;i++){
				HighProteinMorning.getProteinTabItems3().get(i).click();
				Thread.sleep(1000);
				Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
						Thread.currentThread().getStackTrace()[1].getMethodName(), "08-TimingMattersForAHighProteinBreakfast" + i);
				HighProteinMorning.getProteinTabItems3().get(i).click();
				Thread.sleep(1000);
			}
			
			

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "99-ERROR");
			Assert.assertTrue(false, e.getMessage());

		}
	}
	
	
	
	@Test()
	public void test_BreakfastInspiration() throws Exception {
		try {
			HighProteinMorningPage HighProteinMorning = new HighProteinMorningPage(driver);
	   //     driver.navigate().to(domain + path);
			Thread.sleep(1000);
			Common.scrollTo(HighProteinMorning.getLeadingCopy());
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "01-NoClicking");

			HighProteinMorning.getFirstModule().click();
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "02-ClickFirstModule");
			HighProteinMorning.getATag().click();
			Thread.sleep(1000);
			
			HighProteinMorning.getSecondModule().click();
			Thread.sleep(2000);
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "03-ClickSecondModule");
			HighProteinMorning.getATag().click();
			Thread.sleep(1000);

			HighProteinMorning.getThirdModule().click();
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "04-ClickThirdModule");
			HighProteinMorning.getATag().click();
			Thread.sleep(1000);
			Common.scrollTo(HighProteinMorning.getLeadingCopy());
			HighProteinMorning.getResetBtn().click();
			Thread.sleep(1000);
			HighProteinMorning.getOneTheGo().click();
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "05-ClickOnTheGo");
			HighProteinMorning.getResetBtn().click();
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "06-ClickResetBtn");
			
			HighProteinMorning.getSmoothies().click();
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "07-ClickSmoothies");
			HighProteinMorning.getResetBtn().click();
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "08-ClickResetBtn");
			
			HighProteinMorning.getKidFriendly().click();
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "09-ClickKidFriendly");
			HighProteinMorning.getResetBtn().click();
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "10-ClickResetBtn");
			
			HighProteinMorning.getAllFilters().click();
			Thread.sleep(2000);
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "11-ClickAllFilters");
			HighProteinMorning.getVegetarian().click();
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "12-ClickOptioninFilters");

			HighProteinMorning.getCloseAllFilters().click();
			Thread.sleep(2000);
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "13-ClickCloseAllFilters");

			HighProteinMorning.getResetBtn().click();
			Common.scrollTo(HighProteinMorning.getAnotherFirstModule());
			
			HighProteinMorning.getNextPage().click();
			Thread.sleep(2000);
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "14-ClickNextBtn");
			HighProteinMorning.getPreviousPage().click();
			Thread.sleep(2000);
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "15-ClickPreviousBtn");
			
			
			Common.scrollTo(HighProteinMorning.getCopy());
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "16-ProteinGallery");
			HighProteinMorning.getChildImage().click();
			driver.switchTo()
			.window((String) driver.getWindowHandles().toArray()[driver.getWindowHandles().size() - 1]);
			Thread.sleep(20000);
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "17-ExternalLink1");
			driver.close();
			driver.switchTo().window((String) driver.getWindowHandles().toArray()[0]);
			HighProteinMorning.getFruitImage().click();
			driver.switchTo()
			.window((String) driver.getWindowHandles().toArray()[driver.getWindowHandles().size() - 1]);
			Thread.sleep(20000);
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "18-ExternalLink2");
			driver.close();
			driver.switchTo().window((String) driver.getWindowHandles().toArray()[0]);
			Common.scrollTo(HighProteinMorning.getCopy());
			HighProteinMorning.getLeftBtn().click();
			Thread.sleep(4000);
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "19-ClickLeftBtn");
			
			HighProteinMorning.getRightBtn().click();
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "20-ClickRightBtn");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "99-ERROR");
			Assert.assertTrue(false, e.getMessage());

		}
	}
	
	
	
}
