package com.testcase;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.dataprovider.TestDataProvider;
import com.web.Common;

import com.pageobject.HomePage;

public class Home_Test {

	public static String startTime = null;
	public WebDriver driver = null;
	public static String domain = null;
	public static String browser = null;
	public static String market = null;
	public static String width = null;
	public static String height = null;
	private static String path = "";

	@Parameters({ "environment" })
	@BeforeSuite
	public void beforeSuite(@Optional("https://milklife.com") String environment) throws Exception {
		SimpleDateFormat datetime = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		startTime = datetime.format(new Date());
		domain = environment;
	}

	@AfterSuite
	public void afterSuite() throws Exception {
		// MailUtility.sendMail();
	}

	@Parameters({ "browser", "market", "width", "height" })
	@BeforeClass
	public void beforeClass(@Optional("Chrome") String _browser, @Optional("en") String _market, @Optional("1366") String _width, @Optional("768") String _height) throws Exception {
		browser = _browser;
		market = _market;
		width = _width;
		height = _height;
		driver = Common.openBrowser(browser, "about:blank", width, height);
		
	}

	@AfterClass
	public void afterClass() throws Exception {
		driver.quit();
		
	}

	@BeforeMethod
	public void beforeMethod() throws Exception {

	}

	@AfterMethod
	public void afterMethod() throws Exception {
		
	}

	@Test
	public void test_HomePageLoad() throws Exception {
		try {
			
			driver.navigate().to(domain + path);
			Common.ScrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-HomePageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 120000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test
	public void test_HomePageHeroSwitch() throws Exception {
		try {
			HomePage homepage = new HomePage(driver);
			driver.navigate().to(domain + path);
			
			Common.scrollTo(0);
			for(int i=0;i<3&&0!=getHeroIndex(homepage.getHero().findElement(By.className("owl-wrapper")).getAttribute("style"));i++){
				homepage.getNext().click();
				Thread.sleep(2000);
			}
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-HeroFirst");
			
			homepage.getHero().click();
			Thread.sleep(5000);
			Common.ScrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-ClickHeroFirst");
			
			driver.navigate().back();
			
			for(int i=0;i<3&&1!=getHeroIndex(homepage.getHero().findElement(By.className("owl-wrapper")).getAttribute("style"));i++){
				homepage.getNext().click();
				Thread.sleep(2000);
			}
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"03-HeroEven");
			
			homepage.getHero().click();
			Thread.sleep(5000);
			Common.ScrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"04-ClickHeroEven");
			
			driver.navigate().back();
			
			for(int i=0;i<3&&2!=getHeroIndex(homepage.getHero().findElement(By.className("owl-wrapper")).getAttribute("style"));i++){
				homepage.getNext().click();
				Thread.sleep(2000);
			}
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"05-HeroLast");
			
			homepage.getHero().click();
			Thread.sleep(5000);
			Common.ScrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"06-ClickHeroLast");
			
			driver.navigate().back();
			
			homepage.getPrev().click();
			Thread.sleep(2000);
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"07-ClickPrev");
			
			homepage.getNext().click();
			Thread.sleep(2000);
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"08-ClickNext");
			
			if(0==getHeroIndex(homepage.getHero().findElement(By.className("owl-wrapper")).getAttribute("style"))){
				homepage.getFirstHeroScrollForMore().click();
			}
			else if(1==getHeroIndex(homepage.getHero().findElement(By.className("owl-wrapper")).getAttribute("style"))){
				homepage.getEvenHeroScrollForMore().click();
			}
			else if(2==getHeroIndex(homepage.getHero().findElement(By.className("owl-wrapper")).getAttribute("style"))){
				homepage.getLastHeroScrollForMore().click();
			}
			Thread.sleep(2000);
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"09-ClickScrollForMore");
					
			Assert.assertTrue(homepage.getHero().isEnabled(), "Hero is not enabled");
			Assert.assertTrue(homepage.getPrev().isEnabled(), "Prev is not enabled");
			Assert.assertTrue(homepage.getNext().isEnabled(), "Next is not enabled");
			Assert.assertTrue(homepage.getFirstHeroScrollForMore().isEnabled(), "ScrollForMore is not enabled");
			Assert.assertTrue(homepage.getEvenHeroScrollForMore().isEnabled(), "ScrollForMore is not enabled");
			Assert.assertTrue(homepage.getLastHeroScrollForMore().isEnabled(), "ScrollForMore is not enabled");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test()
	public void test_HomePageDonateMilk() throws Exception {
		try {
			HomePage homepage = new HomePage(driver);
			driver.navigate().to(domain + path);
			
			Common.scrollTo(homepage.getBanner());
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Banner");
			
			homepage.getDonateMilk().click();
			Thread.sleep(5000);
			Common.ScrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-ClickDonateMilk");
			
			driver.navigate().back();
			
			Assert.assertTrue(homepage.getBanner().isEnabled(), "Banner is not enabled");
			Assert.assertTrue(homepage.getDonateMilk().isEnabled(), "DonateMilk is not enabled");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test()
	public void test_HomePageFeaturedContent() throws Exception {
		try {
			HomePage homepage = new HomePage(driver);
			driver.navigate().to(domain + path);
			
			Common.scrollTo(homepage.getFeaturedContentTitle());
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-FeaturedContent");
			
			String screenshotname = null;
			for(int i=0;i<3;i++){
				screenshotname = homepage.getFeaturedContentItems().get(i).getText();
				homepage.getFeaturedContentItems().get(i).click();
				Thread.sleep(5000);
				Common.ScrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"02-Click "+screenshotname);
				driver.navigate().to(domain + path);
				Common.scrollTo(homepage.getFeaturedContentTitle());
			}
			
			Assert.assertTrue(homepage.getFeaturedContentTitle().isEnabled(), "FeaturedContentTitle is not enabled");
			Assert.assertTrue(!homepage.getFeaturedContentItems().isEmpty(), "FeaturedContentItems are empty");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test()
	public void test_HomePageViewsRows() throws Exception {
		try {
			HomePage homepage = new HomePage(driver);
			driver.navigate().to(domain + path);
			
			Common.scrollTo(homepage.getViewsRowFirst());
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-ViewsRowFirst");
			
			String screenshotname = homepage.getViewsRowFirstButton().getText();
			homepage.getViewsRowFirstButton().click();
			Thread.sleep(5000);
			Common.ScrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-Click "+screenshotname);
			driver.navigate().back();
			
			Common.scrollTo(homepage.getViewsRowEven());
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"03-ViewsRowEven");
			
			screenshotname = homepage.getViewsRowEvenButton().getText();
			homepage.getViewsRowEvenButton().click();
			Thread.sleep(5000);
			Common.ScrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"04-Click "+screenshotname);
			driver.navigate().back();
			
			Common.scrollTo(homepage.getViewsRowLast());
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"05-ViewsRowLast");
			
			screenshotname = homepage.getViewsRowLastButton().getText();
			homepage.getViewsRowLastButton().click();
			Thread.sleep(5000);
			Common.ScrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"06-Click "+screenshotname);
			driver.navigate().back();
			
			Assert.assertTrue(homepage.getViewsRowFirst().isEnabled(), "ViewsRowFirst is not enabled");
			Assert.assertTrue(homepage.getViewsRowFirstButton().isEnabled(), "ViewsRowFirstButton is not enabled");
			Assert.assertTrue(homepage.getViewsRowEven().isEnabled(), "ViewsRowFirst is not enabled");
			Assert.assertTrue(homepage.getViewsRowEvenButton().isEnabled(), "ViewsRowFirstButton is not enabled");
			Assert.assertTrue(homepage.getViewsRowLast().isEnabled(), "ViewsRowFirst is not enabled");
			Assert.assertTrue(homepage.getViewsRowLastButton().isEnabled(), "ViewsRowFirstButton is not enabled");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test
	public void test_HomePageWhatsHappening() throws Exception {
		try {
			HomePage homepage = new HomePage(driver);
			driver.navigate().to(domain + path);
			
			Common.scrollTo(homepage.getWhatsHappening());
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-WhatsHappening");
			
			homepage.getMostReviewed().click();
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-ClickMostReviewed");
			
			homepage.getMostRecent().click();
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"03-ClickMostRecent");
			
			String recipename = null;
			for(int i=0;i<6;i++){
				recipename = homepage.getRecipes().get(i).getText();
				homepage.getRecipes().get(i).click();
				Common.ScrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"04-Click "+recipename);
				driver.navigate().back();
				Thread.sleep(5000);
				Common.scrollTo(homepage.getWhatsHappening());
			}
			
			Common.scrollTo(homepage.getLoadmore());
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"05-Loadmore");
			
			homepage.getLoadmore().click();
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"06-ClickLoadmoreLoading");
			
			Thread.sleep(5000);
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"07-ClickLoadmoreLoaded");
			
			Assert.assertTrue(homepage.getWhatsHappening().isEnabled(), "WhatsHappening is not enabled");
			Assert.assertTrue(homepage.getMostReviewed().isEnabled(), "MostReviewed is not enabled");
			Assert.assertTrue(homepage.getMostRecent().isEnabled(), "MostRecent is not enabled");
			Assert.assertTrue(homepage.getLoadmore().isEnabled(), "Loadmore is not enabled");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test()
	public void test_HomePageAboutMilkLife() throws Exception {
		try {
			HomePage homepage = new HomePage(driver);
			driver.navigate().to(domain + path);
			
			Common.scrollTo(homepage.getAboutMilkLifeTitle());
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-AboutMilkLife");
			
			homepage.getReadmore().click();
			Thread.sleep(5000);
			Common.ScrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-ClickReadmore");
			
			driver.navigate().back();
			
			Assert.assertTrue(homepage.getAboutMilkLifeTitle().isEnabled(), "AboutMilkLifeTitle is not enabled");
			Assert.assertTrue(homepage.getReadmore().isEnabled(), "Readmore is not enabled");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test()
	public void test_HomePageMyMorningProteinGallery() throws Exception {
		try {
			HomePage homepage = new HomePage(driver);
			driver.navigate().to(domain + path);
			
			Common.scrollTo(homepage.getMyMorningProteinGalleryTitle());
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-MyMorningProteinGallery");
			
			int index = 1;
			homepage.getActiveGalleryItems().get(index).click();
			driver.switchTo().window(driver.getWindowHandles().toArray()[driver.getWindowHandles().size()-1].toString());
			Thread.sleep(15000);
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-ClickGallery"+(index++));
			driver.close();
			Thread.sleep(3000);
			driver.switchTo().window(driver.getWindowHandles().toArray()[0].toString());
			
			homepage.getActiveGalleryItems().get(index).click();
			driver.switchTo().window(driver.getWindowHandles().toArray()[driver.getWindowHandles().size()-1].toString());
			Thread.sleep(15000);
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-ClickGallery"+(index++));
			driver.close();
			Thread.sleep(3000);
			driver.switchTo().window(driver.getWindowHandles().toArray()[0].toString());
			
			/*
			String start = new String(homepage.getMyMorningProteinGallery().getAttribute("style").split(";")[0]);
			homepage.getMyMorningProteinGalleryRightButton().click();
			Thread.sleep(3000);
			while(!homepage.getMyMorningProteinGallery().getAttribute("style").split(";")[0].equals(start)){
				
				homepage.getActiveGalleryItems().get(2).click();
				driver.switchTo().window(driver.getWindowHandles().toArray()[driver.getWindowHandles().size()-1].toString());
				Thread.sleep(15000);
				Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"02-ClickGallery"+(index++));
				driver.close();
				Thread.sleep(3000);
				driver.switchTo().window(driver.getWindowHandles().toArray()[0].toString());
				
				homepage.getMyMorningProteinGalleryRightButton().click();
				Thread.sleep(3000);
			}
			*/
			
			homepage.getMyMorningProteinGalleryRightButton().click();
			Thread.sleep(3000);
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"03-ClickMyMorningProteinGalleryRightButton");
			
			homepage.getMyMorningProteinGalleryLeftButton().click();
			Thread.sleep(3000);
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"04-ClickMyMorningProteinGalleryLeftButton");
			
			Assert.assertTrue(homepage.getMyMorningProteinGalleryTitle().isEnabled(), "MyMorningProteinGalleryTitle is not enabled");
			Assert.assertTrue(homepage.getMyMorningProteinGallery().isEnabled(), "MyMorningProteinGallery is not enabled");
			Assert.assertTrue(homepage.getMyMorningProteinGalleryRightButton().isEnabled(), "MyMorningProteinGalleryRightButton is not enabled");
			Assert.assertTrue(homepage.getMyMorningProteinGalleryLeftButton().isEnabled(), "MyMorningProteinGalleryLeftButton is not enabled");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	private int getHeroIndex(String style){
		if(style.contains("translate3d")){
			if(style.contains("translate3d(0")){
				return 0;
			}else if(style.contains("translate3d(-1")){
				return 1;
			}else if(style.contains("translate3d(-2")){
				return 2;
			}
		}else {
			if(style.contains("left: 0")){
				return 0;
			}else if(style.contains("left: -1")){
				return 1;
			}else if(style.contains("left: -2")){
				return 2;
			}
		}
		return 0;
	}
}
