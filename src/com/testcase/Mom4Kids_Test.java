package com.testcase;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.pageobject.Mom4KidsPage;
import com.web.Common;

public class Mom4Kids_Test {

	public static String startTime = null;
	public WebDriver driver = null;
	public static String domain = null;
	public static String browser = null;
	public static String market = null;
	public static String width = null;
	public static String height = null;
	private static String path = "/activities-kids-recipes";

	@Parameters({ "environment" })
	@BeforeSuite
	public void beforeSuite(@Optional("https://milklife.com") String environment) throws Exception {
		SimpleDateFormat datetime = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		startTime = datetime.format(new Date());
		domain = environment;
	}

	@AfterSuite
	public void afterSuite() throws Exception {
		// MailUtility.sendMail();
	}

	@Parameters({ "browser", "market", "width", "height" })
	@BeforeClass
	public void beforeClass(@Optional("Chrome") String browser, @Optional("en") String market,
			@Optional("1366") String width, @Optional("768") String height) throws Exception {
		Mom4Kids_Test.browser = browser;
		Mom4Kids_Test.market = market;
		Mom4Kids_Test.width = width;
		Mom4Kids_Test.height = height;
		driver = Common.openBrowser(browser, "about:blank", width, height);

	}

	@AfterClass
	public void afterClass() throws Exception {
		driver.quit();

	}

	@BeforeMethod
	public void beforeMethod() throws Exception {

	}

	@AfterMethod
	public void afterMethod() throws Exception {

	}

	@Test
	public void Test_Mom4KidsLoad() throws Exception {
		try {

			driver.navigate().to(domain + path);
			Common.ScrollDownAndTakeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height,
					startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), "01-Mom4KidsLoad");

			Assert.assertTrue(driver.getPageSource().length() > 120000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "99-ERROR");
			Assert.assertTrue(false, e.getMessage());

		}
	}
	
	
	
	@Test()
	public void test_GoNow() throws Exception {
		try {
			Mom4KidsPage Mom4KidsPage = new Mom4KidsPage(driver);
		//	driver.navigate().to(domain + path);
		//	Thread.sleep(1000);
			Common.scrollTo(Mom4KidsPage.getHeroImage());
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "01-GoNowBtn");
			Mom4KidsPage.getGoNowBtn().click();
			Thread.sleep(4000);
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height,
					startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), "02-ClickGoNow");

			driver.navigate().back();

			Assert.assertTrue(Mom4KidsPage.getGoNowBtn().isEnabled(), "GoNow button is not enabled");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "99-ERROR");
			Assert.assertTrue(false, e.getMessage());

		}
	}
	
	
	
	@Test()
	public void test_FeaturedContent() throws Exception {
		try {
			Mom4KidsPage Mom4KidsPage = new Mom4KidsPage(driver);
	    //    driver.navigate().to(domain + path);

			Common.scrollTo(Mom4KidsPage.getHeroImage());
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "01-FeaturedContent");
			
			
			for(int i=0;i<3;i++){
			//	String screenshotname = Mom4KidsPage.getFeatureLinks().get(i)
				Mom4KidsPage.getFeatureLinks().get(i).click();
				Thread.sleep(1000);
				Common.ScrollDownAndTakeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height,
						startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), "02-Click" +"link" +i);
				driver.navigate().back();
				Thread.sleep(1000);
			}
			

			Assert.assertTrue(!Mom4KidsPage.getFeatureLinks().isEmpty(), "Feature Links are empty");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "99-ERROR");
			Assert.assertTrue(false, e.getMessage());

		}
	}
	@Test
	public void test_Activities() throws Exception {
		try {
			Mom4KidsPage Mom4KidsPage = new Mom4KidsPage(driver);
	    //	driver.navigate().to(domain + path);
			Common.scrollTo(Mom4KidsPage.getActivitiesTitle());
			// Most Viewed Button
			Mom4KidsPage.getMostViewBtn().click();
			Thread.sleep(1000);
			Common.scrollTo(Mom4KidsPage.getActivitiesTitle());
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "01-ClickMostViewed");

			// Most Recent Button
			Common.scrollTo(Mom4KidsPage.getActivitiesTitle());
			Mom4KidsPage.getMostRecentBtn().click();
			Thread.sleep(1000);
			Common.scrollTo(Mom4KidsPage.getActivitiesTitle());
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "02-ClickMostRecent");

			
            String recipename= null;
			for (int i = 0; i < 6; i++) {
				recipename = Mom4KidsPage.getArticleLinks().get(i).getText();
				Mom4KidsPage.getArticleLinks().get(i).click();
				Thread.sleep(2000);
				Common.ScrollDownAndTakeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height,
						startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), "03-Click " + recipename);
				driver.navigate().back();
				Thread.sleep(2000);
				Common.scrollTo(Mom4KidsPage.getActivitiesTitle());
			}

			// click load more button
			Common.scrollTo(Mom4KidsPage.getLoadMoreBtn());
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "04-Loadmore");
		
			Mom4KidsPage.getLoadMoreBtn().click();
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "05-ClickLoadmoreLoading");

			Thread.sleep(5000);
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "06-ClickLoadmoreLoaded");

			Assert.assertTrue(Mom4KidsPage.getActivitiesTitle().isEnabled(), "TastyTips is not enabled");
			Assert.assertTrue(Mom4KidsPage.getMostRecentBtn().isEnabled(), "MostReviewed is not enabled");
			Assert.assertTrue(Mom4KidsPage.getMostViewBtn().isEnabled(), "MostRecent is not enabled");
			Assert.assertTrue(Mom4KidsPage.getLoadMoreBtn().isEnabled(), "Loadmore is not enabled");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "99-ERROR");
			Assert.assertTrue(false, e.getMessage());

		}
	}

	
	
}
