package com.testcase;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.pageobject.GivePage;
import com.web.Common;

public class Give_Test {

	public static String startTime = null;
	public WebDriver driver = null;
	public static String domain = null;
	public static String browser = null;
	public static String market = null;
	public static String width = null;
	public static String height = null;
	private static String path = "";
	
	@Parameters({ "environment" })
	@BeforeSuite
	public void beforeSuite(@Optional("https://milklife.com/give") String environment) throws Exception {
		SimpleDateFormat datetime = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		startTime = datetime.format(new Date());
		domain = environment;
	}

	@AfterSuite
	public void afterSuite() throws Exception {
		// MailUtility.sendMail();
	}

	@Parameters({ "browser", "market", "width", "height" })
	@BeforeClass
	public void beforeClass(@Optional("chrome") String _browser, @Optional("en") String _market, @Optional("1366") String _width, @Optional("768") String _height) throws Exception {
		browser = _browser;
		market = _market;
		width = _width;
		height = _height;
		driver = Common.openBrowser(browser, "about:blank", width, height);
		
	}

	@AfterClass
	public void afterClass() throws Exception {
		driver.quit();
		
	}

	@BeforeMethod
	public void beforeMethod() throws Exception {

	}

	@AfterMethod
	public void afterMethod() throws Exception {
		
	}

	@Test
	public void test_GivePageLoad() throws Exception {
		try {
			
			driver.navigate().to(domain + path);
			Common.ScrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-GivePageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 120000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	@Test()
	public void test_GiveNow() throws Exception {
		try {
			GivePage give = new GivePage(driver);
			driver.navigate().to(domain + path);
			
			Common.scrollTo(give.getGiveHero());
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-GiveNowButton");
			
			give.getGiveNow().click();
			Thread.sleep(5000);
			Common.ScrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-ClickGiveNow");
			
			driver.navigate().back();
			
			Assert.assertTrue(give.getGiveNow().isEnabled(), "GiveNow is not enabled");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	@Test()
	public void test_WatchVideo() throws Exception {
		try {
			GivePage give = new GivePage(driver);
			driver.navigate().to(domain + path);
			
			Common.scrollTo(give.getWatchVideo());
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-WatchVideoButton");
			
			give.getWatchVideo().click();
			Thread.sleep(5000);
			Common.ScrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-ClickGiveNowWatchVideo");
			
			driver.navigate().back();
			
			Assert.assertTrue(give.getGiveNow().isEnabled(), "WatchVideo is not enabled");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	@Test
	public void test_GiveMap() throws Exception {
		try {
			GivePage give = new GivePage(driver);
			driver.navigate().to(domain + path);
			
			
		   Common.scrollTo(give.getTotal());
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Map");
			
			
			for(int i=0;i<51;i++){
				//if(isClickable(give.getMap().get(i), driver)) {
				if(i != 8 && i !=9 && i !=11 && i !=22 && i !=30) {
					give.getMap().get(i).click();
					Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						i+"-Click");
					/*Common.scrollTo(give.getFood());
					Thread.sleep(5000);
					Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
							i+"-ClickChange");
					Common.scrollTo(give.getTotal());
				*/}
				//}
				
				}
			
		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test
	public void test_Text() throws Exception {
		try {
			GivePage give = new GivePage(driver);
			driver.navigate().to(domain + path);
			
			Common.scrollTo(give.getTotal());
			give.getMap().get(0).click();
			Assert.assertEquals(give.getName().getText(), "Alabama");
			//Assert.assertEquals(give.getLogoName().getText(), "Birmingham");
			
		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
		}
		}
	
	
	
	@Test
	public void test_Check() throws Exception {
		try {
			GivePage give = new GivePage(driver);
			driver.navigate().to(domain + path);
			
			
		    Common.scrollTo(give.getCheck());
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Check");
			
			give.getCheck1().click();
			Thread.sleep(5000);
			driver.switchTo().window((String) driver.getWindowHandles().toArray()[driver.getWindowHandles().size()-1]);
			Common.ScrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Click");
			
			driver.close();
			driver.switchTo().window((String) driver.getWindowHandles().toArray()[0]);
			
			 Common.scrollTo(give.getCheck());
			
			    give.getCheck2().click();
				Thread.sleep(5000);
				driver.switchTo().window((String) driver.getWindowHandles().toArray()[driver.getWindowHandles().size()-1]);
				Common.ScrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"02-Click");
				
				driver.close();
				driver.switchTo().window((String) driver.getWindowHandles().toArray()[0]);
				
				 Common.scrollTo(give.getCheck());
					give.getCheck3().click();
					Thread.sleep(5000);
					driver.switchTo().window((String) driver.getWindowHandles().toArray()[driver.getWindowHandles().size()-1]);
					Common.ScrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
							"03-Click");
					
					driver.close();
					driver.switchTo().window((String) driver.getWindowHandles().toArray()[0]);
			
			
			
			Assert.assertTrue(give.getCheck1().isEnabled(), "Check1 is not enabled");
			Assert.assertTrue(give.getCheck1().isEnabled(), "Check2 is not enabled");
			Assert.assertTrue(give.getCheck1().isEnabled(), "Check3 is not enabled");
			
		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	/*private boolean isClickable(WebElement el, WebDriver driver) {
		try{
			WebDriverWait wait = new WebDriverWait(driver, 1);
			wait.until(ExpectedConditions.elementToBeClickable(el));
			return true;
		}
		catch (Exception e){
			return false;
		}
	}*/

}
