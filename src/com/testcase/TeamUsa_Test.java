package com.testcase;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.pageobject.TeamUsa;
import com.pageobject.HomePage;
import com.web.Common;
public class TeamUsa_Test {
	public static String startTime = null;
	public WebDriver driver = null;
	public static String domain = null;
	public static String browser = null;
	public static String market = null;
	public static String width = null;
	public static String height = null;
	private static String path = "/teamusa";
	
	@Parameters({ "environment" })
	@BeforeSuite
	public void beforeSuite(@Optional("https://milklife.com")String environment) throws Exception {
		SimpleDateFormat datetime = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		startTime = datetime.format(new Date());
		domain = environment;
	}
	@AfterSuite
	public void afterSuite() throws Exception {
		// MailUtility.sendMail();
	}
	
	@Parameters({ "browser", "market", "width", "height" })
	@BeforeClass
	public void beforeClass(@Optional("Chrome")String browser, @Optional("en")String market, @Optional("1366")String width, @Optional("768")String height) throws Exception {
		TeamUsa_Test.browser = browser;
		TeamUsa_Test.market = market;
		TeamUsa_Test.width = width;
		TeamUsa_Test.height = height;
		driver = Common.openBrowser(browser, "about:blank", width, height);
		
	}
	
	@AfterClass
	public void afterClass() throws Exception {
		driver.quit();
		
	}
	
	@BeforeMethod
	public void beforeMethod() throws Exception {

	}

	@AfterMethod
	public void afterMethod() throws Exception {
		
	}
	@Test
	public void test_TeamUsaPageLoad() throws Exception {
		try {
			
			driver.navigate().to(domain + path);
			Common.ScrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-FunBreakfastIdeasPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 120000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	//ToDo: Please add test cases for TeamUsaPage
	
	@Test()
	public void test_FindOutButton() throws Exception {
		try {
			TeamUsa teamUsa = new TeamUsa(driver);
			driver.navigate().to(domain + path);
			
		    Common.scrollTo(teamUsa.getTextBlock());
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-TextBlock");
			
			teamUsa.getFindOut().click();
			Thread.sleep(5000);
			Common.ScrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-ClickFindOut");
			
			driver.navigate().back();
			
			Assert.assertTrue(teamUsa.getTextBlock().isEnabled(), "TextBlock is not enabled");
			Assert.assertTrue(teamUsa.getFindOut().isEnabled(), "FindOut is not enabled");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test()
	public void test_SeeHow() throws Exception {
		try {
			TeamUsa teamUsa = new TeamUsa(driver);
			driver.navigate().to(domain + path);
			
		    Common.scrollTo(teamUsa.getFindOut());
		    Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-FindOut");
			
			teamUsa.getSeeHow().click();
			Thread.sleep(5000);
			Common.ScrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-ClickSeeHow");
			
			driver.navigate().back();
			
			Assert.assertTrue(teamUsa.getFindOut().isEnabled(), "FindOut is not enabled");
			Assert.assertTrue(teamUsa.getSeeHow().isEnabled(), "SeeHow is not enabled");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test()
	public void test_WatchThis() throws Exception {
		try {
			TeamUsa teamUsa = new TeamUsa(driver);
			driver.navigate().to(domain + path);
			
		    Common.scrollTo(teamUsa.getFindOut());
		    Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-FindOut");
			
			teamUsa.getWatchThis().click();
			Thread.sleep(5000);
			Common.ScrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-ClickWatchThis");
			
			driver.navigate().back();
			
			Assert.assertTrue(teamUsa.getFindOut().isEnabled(), "FindOut is not enabled");
			Assert.assertTrue(teamUsa.getWatchThis().isEnabled(), "WatchThis is not enabled");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	@Test()
	public void test_MilkMatters() throws Exception {
		try {
			TeamUsa teamUsa = new TeamUsa(driver);
			driver.navigate().to(domain + path);
			
		    Common.scrollTo(teamUsa.getFindOut());
		    Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-FindOut");
			
			teamUsa.getMilkMatters().click();
			Thread.sleep(5000);
			Common.ScrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-ClickMilkMatters");
			
			driver.navigate().back();
			
			Assert.assertTrue(teamUsa.getFindOut().isEnabled(), "FindOut is not enabled");
			Assert.assertTrue(teamUsa.getMilkMatters().isEnabled(), "MilkMatters is not enabled");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test
	public void test_MoreFromTeamMlik() throws Exception {
		try {
			TeamUsa teamUsa = new TeamUsa(driver);
			driver.navigate().to(domain + path);
			
			Common.scrollTo(teamUsa.getMoreFromTeamMlik());
		    Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-MoreFromTeamMlik");
			
		    teamUsa.getMostViewed().click();
			Common.scrollTo(teamUsa.getMoreFromTeamMlik());
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-ClickMostVeviewed");
			
			teamUsa.getMostRecent().click();
			Common.scrollTo(teamUsa.getMoreFromTeamMlik());
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"03-ClickMostRecent");
			
			String recipename = null;
			for(int i=0;i<6;i++){
				recipename = teamUsa.getRecipes().get(i).getText().replace("?", "");
				teamUsa.getRecipes().get(i).click();
				Common.ScrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"04-Click "+recipename);
				driver.navigate().back();
			}			
			Assert.assertTrue(teamUsa.getMostViewed().isEnabled(), "MostReviewed is not enabled");
			Assert.assertTrue(teamUsa.getMostRecent().isEnabled(), "MostRecent is not enabled");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	@Test
	public void test_LoadMoreLink() throws Exception {
		try {
			TeamUsa teamUsa = new TeamUsa(driver);
			driver.navigate().to(domain + path);
		    Thread.sleep(2000);
			Common.scrollTo(teamUsa.getLoadmore());
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"05-Loadmore");
			
			teamUsa.getLoadmore().click();
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"06-ClickLoadmoreLoading");
			
			Thread.sleep(5000);
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"07-ClickLoadmoreLoaded");
			Assert.assertTrue(teamUsa.getLoadmore().isEnabled(), "Loadmore is not enabled");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
}
