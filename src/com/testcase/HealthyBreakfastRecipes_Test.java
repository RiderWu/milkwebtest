package com.testcase;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.By.ById;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.dataprovider.TestDataProvider;
import com.web.Common;
import com.pageobject.HealthyBreakfastRecipesPage;
import com.pageobject.HomePage;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
public class HealthyBreakfastRecipes_Test {

	public static String StartTime = null;
	public WebDriver driver = null;
	public static String Domain = null;
	public static String Browser = null;
	public static String Market = null;
	public static String Width = null;
	public static String Height = null;
	private static String Path = "/healthy-breakfast-recipes";

	@Parameters({ "environment" })
	@BeforeSuite
	public void beforeSuite(@Optional("https://milklife.com/")String environment) throws Exception {
		SimpleDateFormat datetime = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		StartTime = datetime.format(new Date());
		Domain = environment;
	}

	@AfterSuite
	public void afterSuite() throws Exception {
		// MailUtility.sendMail();
	}

	@Parameters({ "browser", "market", "width", "height" })
	@BeforeClass
	public void beforeClass(@Optional("Firefox")String browser, @Optional("en")String market, @Optional("1366")String width, @Optional("768")String height) throws Exception {
		Browser = browser;
		Market = market;
		Width = width;
		Height = height;
		driver = Common.openBrowser(Browser, "about:blank", width, height);
		
	}

	@AfterClass
	public void afterClass() throws Exception {
		driver.quit();
		
	}

	@BeforeMethod
	public void beforeMethod() throws Exception {

	}

	@AfterMethod
	public void afterMethod() throws Exception {
		
	}

	@Test
	public void Test_HealthyBreakfastRecipesPageLoad() throws Exception {
		try {
			
			driver.navigate().to(Domain + Path);
			Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-HealthyBreakfastRecipesPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 60000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
		finally
		{
			((JavascriptExecutor) driver).executeScript("window.scrollBy(0,0)");
		}
	}
	
	//ToDo: Please add test cases for HealthyBreakfastRecipesPage
	
	@Test()
	public void Test_HealthyBreakfastRecipesPageBatterUp() throws Exception {
		try {
			HealthyBreakfastRecipesPage healthyBreakfastRecipesPage = new HealthyBreakfastRecipesPage(driver);
			driver.navigate().to(Domain + Path);
	
			Common.scrollTo(healthyBreakfastRecipesPage.getBatterUp().getLocation().getY()-100);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Button");

			healthyBreakfastRecipesPage.getBatterUp().click();
			
			Thread.sleep(5000);
			Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-ClickBatterUp");
			
			driver.navigate().back();
			
			Assert.assertTrue(healthyBreakfastRecipesPage.getBatterUp().isEnabled(), "BatterUp is not enabled");
	
		} catch (Exception e) {
	
			e.printStackTrace();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}

	@Test()
	public void Test_HealthyBreakfastRecipesPageLetsTalkProtein() throws Exception {
		try {
			HealthyBreakfastRecipesPage healthyBreakfastRecipesPage = new HealthyBreakfastRecipesPage(driver);
			driver.navigate().to(Domain + Path);
			
			Common.scrollTo(healthyBreakfastRecipesPage.getLetsTalkProtein().getLocation().getY()-100);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-LetsTalkProtein");
			
			healthyBreakfastRecipesPage.getLetsTalkProtein().click();
			
			for(String winHandle:driver.getWindowHandles()){
			    driver.switchTo().window(winHandle);
			}
			
			Thread.sleep(5000);
			Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-ClickLetsTalkProtein");
			
			driver.close();
			for(String winHandle:driver.getWindowHandles()){
			    driver.switchTo().window(winHandle);
			}
			
			Assert.assertTrue(healthyBreakfastRecipesPage.getLetsTalkProtein().isEnabled(), "LetsTalkProtein is not enabled");
	
		} catch (Exception e) {
	
			e.printStackTrace();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test()
	public void Test_HealthyBreakfastRecipesPagePrepareAPeanutButterPuppySandwich() throws Exception {
		try {
			HealthyBreakfastRecipesPage healthyBreakfastRecipesPage = new HealthyBreakfastRecipesPage(driver);
			driver.navigate().to(Domain + Path);

			Common.scrollTo(healthyBreakfastRecipesPage.getPrepareAPeanutButterPuppySandwich().getLocation().getY()-100);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-PrepareAPeanutButterPuppySandwich");
			
			healthyBreakfastRecipesPage.getPrepareAPeanutButterPuppySandwich().click();
			
			for(String winHandle:driver.getWindowHandles()){
			    driver.switchTo().window(winHandle);
			}
			
			Thread.sleep(5000);
			Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-ClickPrepareAPeanutButterPuppySandwich");
			
			driver.close();
			for(String winHandle:driver.getWindowHandles()){
			    driver.switchTo().window(winHandle);
			}
			
			Assert.assertTrue(healthyBreakfastRecipesPage.getPrepareAPeanutButterPuppySandwich().isEnabled(), "PrepareAPeanutButterPuppySandwich is not enabled");
	
		} catch (Exception e) {
	
			e.printStackTrace();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test()
	public void Test_HealthyBreakfastRecipesPageBatterUpLink() throws Exception {
		try {
			HealthyBreakfastRecipesPage healthyBreakfastRecipesPage = new HealthyBreakfastRecipesPage(driver);
			driver.navigate().to(Domain + Path);

			Common.scrollTo(healthyBreakfastRecipesPage.getBatterUpLink().getLocation().getY()-100);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-BatterUpLink");
			
			healthyBreakfastRecipesPage.getBatterUpLink().click();
			
			for(String winHandle:driver.getWindowHandles()){
			    driver.switchTo().window(winHandle);
			}
			
			Thread.sleep(5000);
			Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-ClickBatterUpLink");
			
			driver.close();
			for(String winHandle:driver.getWindowHandles()){
			    driver.switchTo().window(winHandle);
			}
			
			Assert.assertTrue(healthyBreakfastRecipesPage.getBatterUpLink().isEnabled(), "BatterUpLink is not enabled");
	
		} catch (Exception e) {
	
			e.printStackTrace();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	
	@Test
	public void Test_HealthyBreakfastRecipesPageRecipesTitle() throws Exception {
		try {
			HealthyBreakfastRecipesPage healthyBreakfastRecipesPage = new HealthyBreakfastRecipesPage(driver);
			driver.navigate().to(Domain + Path);
			
			Common.scrollTo(healthyBreakfastRecipesPage.getRecipesTitle().getLocation().getY()-50);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-RecipesTitle");
			
			Common.scrollTo(healthyBreakfastRecipesPage.getMostReviewed().getLocation().getY()-50);
			healthyBreakfastRecipesPage.getMostReviewed().click();
			Common.scrollTo(healthyBreakfastRecipesPage.getMostReviewed().getLocation().getY()-50);
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-ClickMostReviewed");
			
			Common.scrollTo(healthyBreakfastRecipesPage.getMostRecent().getLocation().getY()-50);
			healthyBreakfastRecipesPage.getMostRecent().click();
			Common.scrollTo(healthyBreakfastRecipesPage.getMostReviewed().getLocation().getY()-50);
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"03-ClickMostRecent");
			
			Common.scrollTo(healthyBreakfastRecipesPage.getMostRecent().getLocation().getY()-50);
			String recipename = null;
			for(int i=0;i<6;i++){
				recipename = healthyBreakfastRecipesPage.getRecipes().get(i).getText();
				healthyBreakfastRecipesPage.getRecipes().get(i).click();
				Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"04-Click "+recipename);
				driver.navigate().back();
			}
			
			Common.scrollTo(healthyBreakfastRecipesPage.getLoadmore().getLocation().getY()-50);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"05-Loadmore");
			Assert.assertTrue(healthyBreakfastRecipesPage.getLoadmore().isEnabled(), "Loadmore is not enabled");
			
			healthyBreakfastRecipesPage.getLoadmore().click();
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"06-ClickLoadmoreLoading");
			
			Thread.sleep(5000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"07-ClickLoadmoreLoaded");
			
		
			//dropdown
			if (Browser.equals("IE") || Browser.equals("Firefox") ||  Browser.equals("FireFox") ){
				Common.scrollTo(healthyBreakfastRecipesPage.getRecipesType().getLocation().getY()-50);
				String imageName;
				for (int i=0; i <healthyBreakfastRecipesPage.getItems().size(); i++){
					Common.scrollTo(healthyBreakfastRecipesPage.getRecipesTitle());
					
					healthyBreakfastRecipesPage.getRecipesType().click();
					imageName=healthyBreakfastRecipesPage.getItems().get(i).getText();
					Thread.sleep(1000);
					healthyBreakfastRecipesPage.getRecipesType().sendKeys(Keys.DOWN);
					Thread.sleep(1000);
					healthyBreakfastRecipesPage.getItems().get(i).click();
					
					Thread.sleep(7000);
					Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
							"08"+imageName);
				}
			}
			else{
				Common.scrollTo(healthyBreakfastRecipesPage.getRecipesType().getLocation().getY()-50);
				String imageName;
				for (int i=0; i <healthyBreakfastRecipesPage.getItems().size(); i++){
					Common.scrollTo(healthyBreakfastRecipesPage.getRecipesTitle());
					healthyBreakfastRecipesPage.getRecipesType().click();
					Thread.sleep(1000);
					imageName=healthyBreakfastRecipesPage.getItems().get(i).getText();
					healthyBreakfastRecipesPage.getItems().get(i).click();
					Thread.sleep(7000);
					Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
							"08"+imageName);
					
				}
				
			}
			
			
			Assert.assertTrue(healthyBreakfastRecipesPage.getRecipesTitle().isEnabled(), "WhatsHappening is not enabled");
			Assert.assertTrue(healthyBreakfastRecipesPage.getMostReviewed().isEnabled(), "MostReviewed is not enabled");
			Assert.assertTrue(healthyBreakfastRecipesPage.getMostRecent().isEnabled(), "MostRecent is not enabled");
			

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}

}

	