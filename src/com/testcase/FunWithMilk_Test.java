package com.testcase;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.web.Common;
import com.pageobject.FunWithMilkPage;

public class FunWithMilk_Test {

	public static String startTime = null;
	public WebDriver driver = null;
	public static String domain = null;
	public static String browser = null;
	public static String market = null;
	public static String width = null;
	public static String height = null;
	private static String path = "/fun-with-milk";

	@Parameters({ "environment" })
	@BeforeSuite
	public void beforeSuite(@Optional("https://milklife.com") String environment) throws Exception {
		SimpleDateFormat datetime = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		startTime = datetime.format(new Date());
		domain = environment;
	}

	@AfterSuite
	public void afterSuite() throws Exception {
		// MailUtility.sendMail();
	}

	@Parameters({ "browser", "market", "width", "height" })
	@BeforeClass
	public void beforeClass(@Optional("IE") String browser, @Optional("en") String market,
			@Optional("1366") String width, @Optional("768") String height) throws Exception {
		FunWithMilk_Test.browser = browser;
		FunWithMilk_Test.market = market;
		FunWithMilk_Test.width = width;
		FunWithMilk_Test.height = height;
		driver = Common.openBrowser(FunWithMilk_Test.browser, "about:blank", width, height);

	}

	@AfterClass
	public void afterClass() throws Exception {
		driver.quit();

	}

	@BeforeMethod
	public void beforeMethod() throws Exception {

	}

	@AfterMethod
	public void afterMethod() throws Exception {

	}

	@Test
	public void Test_FunWithMilkPageLoad() throws Exception {
		try {

			driver.navigate().to(domain + path);
			Common.ScrollDownAndTakeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height,
					startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), "01-FunWithMilkPageLoad");

			Assert.assertTrue(driver.getPageSource().length() > 120000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "99-ERROR");
			Assert.assertTrue(false, e.getMessage());

		}
	}

	// ToDo: Please add test cases for FunWithMilkPage

	@Test()
	public void test_FarmFresh() throws Exception {
		try {
			FunWithMilkPage funwithmilkpage = new FunWithMilkPage(driver);
	//		driver.navigate().to(domain + path);
			Thread.sleep(5000);
			Common.scrollTo(funwithmilkpage.getFunWithMilkHero());
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "01-FarmFresh");
			funwithmilkpage.getFarmFresh().click();

			Common.ScrollDownAndTakeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height,
					startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), "02-ClickFarmFresh");

			driver.navigate().back();

			// 断言
			Assert.assertTrue(funwithmilkpage.getFarmFresh().isEnabled(), "FarmFresh button is not enabled");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "99-ERROR");
			Assert.assertTrue(false, e.getMessage());

		}
	}

	@Test()
	public void test_FunWithMilkFeaturedContent() throws Exception {
		try {
			FunWithMilkPage funwithmilkpage = new FunWithMilkPage(driver);
	//		driver.navigate().to(domain + path);

			Common.scrollTo(funwithmilkpage.getFunWithMilkHero());
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "01-FeaturedContent");

			funwithmilkpage.getFeaturedContentItems().get(0).click();
			Thread.sleep(5000);
			driver.switchTo()
					.window((String) driver.getWindowHandles().toArray()[driver.getWindowHandles().size() - 1]);
			Common.ScrollDownAndTakeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height,
					startTime, Thread.currentThread().getStackTrace()[1].getMethodName(),
					"02-Click " + "Let's talk protein");
			driver.close();
				driver.switchTo().window((String) driver.getWindowHandles().toArray()[0]);
		//	driver.navigate().back();

			funwithmilkpage.getFeaturedContentItems().get(1).click();
			Thread.sleep(5000);
		//	driver.switchTo()
		//			.window((String) driver.getWindowHandles().toArray()[driver.getWindowHandles().size() - 1]);
			Common.ScrollDownAndTakeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height,
					startTime, Thread.currentThread().getStackTrace()[1].getMethodName(),
					"03-Click " + "4 ways to master your morning routine");
		//	driver.close();
		//	driver.switchTo().window((String) driver.getWindowHandles().toArray()[0]);
			driver.navigate().back();
			
			funwithmilkpage.getFeaturedContentItems().get(2).click();
			Thread.sleep(5000);
			Common.ScrollDownAndTakeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height,
					startTime, Thread.currentThread().getStackTrace()[1].getMethodName(),
					"04-Click " + "tips to save the earth all year long");
			driver.navigate().back();

			Assert.assertTrue(!funwithmilkpage.getFeaturedContentItems().isEmpty(), "FeaturedContentItems are empty");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "99-ERROR");
			Assert.assertTrue(false, e.getMessage());

		}
	}

	@Test
	public void test_FunWithMilkTastyTips() throws Exception {
		try {
			FunWithMilkPage funwithmilkpage = new FunWithMilkPage(driver);
	//	driver.navigate().to(domain + path);
			Common.scrollTo(funwithmilkpage.getFunWithMilkTastyTips());
			// Most Viewed Button
			funwithmilkpage.getFunWithMilkMostViewed().click();
			Thread.sleep(1000);
			Common.scrollTo(funwithmilkpage.getFunWithMilkTastyTips());
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "01-ClickMostViewed");

			// Most Recent Button
			Common.scrollTo(funwithmilkpage.getFunWithMilkTastyTips());
			funwithmilkpage.getFunWithMilkMostRecent().click();
			Thread.sleep(1000);
			Common.scrollTo(funwithmilkpage.getFunWithMilkTastyTips());
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "02-ClickMostRecent");

			// Drop Down Men
			funwithmilkpage.getFunWithMilkStatus().click();
			Thread.sleep(3000);
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "03-ClickStatus");
			funwithmilkpage.getFunWithMilkOption2().click();
			Thread.sleep(5000);
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "04-SelectActivities");

			// continue to click status button
			Common.scrollTo(funwithmilkpage.getFunWithMilkTastyTips());
			funwithmilkpage.getFunWithMilkStatus().click();
			funwithmilkpage.getFunWithMilkOption3().click();
			Thread.sleep(5000);
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "05-SelectCrafts");

			funwithmilkpage.getFunWithMilkStatus().click();
			funwithmilkpage.getFunWithMilkOption4().click();
			Thread.sleep(3000);
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "06-SelectProteinGallery");

			// click links
			// String recipename = null;

			Common.scrollTo(funwithmilkpage.getFunWithMilkTastyTips());
			funwithmilkpage.getFunWithMilkStatus().click();
			funwithmilkpage.getFunWithMilkOption1().click();
			Thread.sleep(8000);
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "07-SelectAny");
			for (int i = 0; i < 6; i++) {
				// recipename = funwithmilkpage.getRecipes().get(i).getText();
				funwithmilkpage.getRecipes().get(i).click();
				Common.ScrollDownAndTakeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height,
						startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), "08-link " + i);
				driver.navigate().back();
		//		funwithmilkpage.getFunWithMilkStatus().click();
		//		funwithmilkpage.getFunWithMilkOption2().click();
				Thread.sleep(4000);
			}

			// click load more button
			Common.scrollTo(funwithmilkpage.getLoadmore());
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "09-Loadmore");
		
			funwithmilkpage.getLoadmore().click();
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "10-ClickLoadmoreLoading");

			Thread.sleep(5000);
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "11-ClickLoadmoreLoaded");

			Assert.assertTrue(funwithmilkpage.getFunWithMilkTastyTips().isEnabled(), "TastyTips is not enabled");
			Assert.assertTrue(funwithmilkpage.getFunWithMilkMostRecent().isEnabled(), "MostReviewed is not enabled");
			Assert.assertTrue(funwithmilkpage.getFunWithMilkMostViewed().isEnabled(), "MostRecent is not enabled");
			Assert.assertTrue(funwithmilkpage.getLoadmore().isEnabled(), "Loadmore is not enabled");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "99-ERROR");
			Assert.assertTrue(false, e.getMessage());

		}
	}

}
