package com.testcase;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.pageobject.MessagesAndHelp;
import com.web.Common;

public class MessagesAndHelp_Test {

	public static String startTime = null;
	public WebDriver driver = null;
	public static String domain = null;
	public static String browser = null;
	public static String market = null;
	public static String width = null;
	public static String height = null;
	private static String path = "";
	public MessagesAndHelp messagesAndHelp = null;
	public String originalWindowHandle;

	@Parameters({ "environment" })
	@BeforeSuite
	public void beforeSuite(String environment) throws Exception {
		SimpleDateFormat datetime = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		startTime = datetime.format(new Date());
		domain = environment;
	}

	@AfterSuite
	public void afterSuite() throws Exception {
		// MailUtility.sendMail();
	}

	@Parameters({ "browser", "market", "width", "height" })
	@BeforeClass
	public void beforeClass(String browser, String market, String width, String height) throws Exception {
		MessagesAndHelp_Test.browser = browser;
		MessagesAndHelp_Test.market = market;
		MessagesAndHelp_Test.width = width;
		MessagesAndHelp_Test.height = height;
		driver = Common.openBrowser(browser, "about:blank", width, height);
		messagesAndHelp = new MessagesAndHelp(driver);
	}

	@AfterClass
	public void afterClass() throws Exception {
		driver.quit();
		
	}

	@BeforeMethod
	public void beforeMethod() throws Exception {

	}

	@AfterMethod
	public void afterMethod() throws Exception {
		
	}
	
	@Test
	public void test_rightBarDisplay() {
		driver.navigate().to(domain + path);
		Assert.assertTrue(messagesAndHelp.isRightBarPresented(), "Right bar isn't appearing...");
	}
	
	@Test
	public void test_sharePageFromRightBar() throws Exception {
		driver.navigate().to(domain + path);
		saveDefaultWindowHandle();
		
		// Facebook
		messagesAndHelp.clickFacebookOnRightBar();
		switchToWin("www.facebook.com");
		Thread.sleep(2000);
		Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
				"01-FacebookPopUpWindow");
		Assert.assertEquals(driver.getTitle(), "Facebook");
		// TODO login Facebook and post
		
		switchToDefaultWindowHandle();
		
		// Twitter
		messagesAndHelp.clickTwitterOnRightBar();
		switchToWin("twitter.com");
		Thread.sleep(2000);
		Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
				"02-TwitterPopUpWindow");
		Assert.assertEquals(driver.getTitle(), "Share a link on Twitter");
		
		switchToDefaultWindowHandle();
		
		// Pinterest
		messagesAndHelp.clickPinterestOnRightBar();
		messagesAndHelp.waitForIFrameAppear();
		Thread.sleep(2000);
		Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
				"03-PinterestIFrame");
		Assert.assertEquals(messagesAndHelp.getPinterestTitle(), "Choose a Pin to save");
	}
	
	
	private void saveDefaultWindowHandle() {
		originalWindowHandle = driver.getWindowHandle();
	}
	
	private void switchToWin(String currentPage) {
		Set<String> handles = driver.getWindowHandles();
		for (String handle : handles) {
			driver.switchTo().window(handle);
			if (driver.getCurrentUrl().contains(currentPage)) {
				break;
			}
		}
	}
	private void switchToDefaultWindowHandle() {
		if (originalWindowHandle != null) {
			driver.switchTo().window(originalWindowHandle);
		}
	}
	
}
