package com.testcase;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.pageobject.Footer;
import com.web.Common;

public class Footer_Test {

	public static String startTime = null;
	public WebDriver driver = null;
	public static String domain = null;
	public static String browser = null;
	public static String market = null;
	public static String width = null;
	public static String height = null;
	private static String path = "";
	public Footer footer = null;

	@Parameters({ "environment" })
	@BeforeSuite
	public void beforeSuite(String environment) throws Exception {
		SimpleDateFormat datetime = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		startTime = datetime.format(new Date());
		domain = environment;
	}

	@AfterSuite
	public void afterSuite() throws Exception {
		// MailUtility.sendMail();
	}

	@Parameters({ "browser", "market", "width", "height" })
	@BeforeClass
	public void beforeClass(String browser, String market, String width, String height) throws Exception {
		Footer_Test.browser = browser;
		Footer_Test.market = market;
		Footer_Test.width = width;
		Footer_Test.height = height;
		driver = Common.openBrowser(browser, "about:blank", width, height);
		footer = new Footer(driver);
		
	}

	@AfterClass
	public void afterClass() throws Exception {
		driver.quit();
		
	}

	@BeforeMethod
	public void beforeMethod() throws Exception {

	}

	@AfterMethod
	public void afterMethod() throws Exception {
		
	}
	
	@Test
	public void test_footerElementsDisplay() {
		driver.navigate().to(domain + path);
		Assert.assertTrue(footer.isFooterMenuPresented(), "Missing elements on Footer menu...");
		Assert.assertTrue(footer.isSocialMediaPresented(), "Missing social meida on Footer...");
	}
	
	@Test
	public void test_externalURLs() {
		driver.navigate().to(domain + path);
		Assert.assertEquals(footer.getFacebookLink(), "https://www.facebook.com/milk");
		Assert.assertEquals(footer.getTwitterLink(), "https://twitter.com/milklife");
		Assert.assertEquals(footer.getPinterestLink(), "http://pinterest.com/milk");
		Assert.assertEquals(footer.getInstagramLink(), "http://instagram.com/milklife");
		Assert.assertEquals(footer.getYoutubeLink(), "http://www.youtube.com/milklife");
		Assert.assertEquals(footer.getSpanishSiteLink(), "https://fuertesconleche.com/");
		Assert.assertEquals(footer.getExternalSiteLink(), "http://builtwithchocolatemilk.com/");
	}
	
	@Test
	public void test_subPageRedirection() throws Exception {
		driver.navigate().to(domain + path);
		
		footer.clickContactUs();
		Thread.sleep(2000);
		Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
				"01-ContactUs");
		driver.navigate().back();
		Thread.sleep(2000);
		
		footer.clickPrivacyPolicy();
		Thread.sleep(2000);
		Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
				"02-PrivacyPolicy");
		driver.navigate().back();
		Thread.sleep(2000);
		
		footer.clickOurPartners();
		Thread.sleep(2000);
		Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
				"03-OurPartners");
		driver.navigate().back();
		Thread.sleep(2000);
		
		footer.clickMilkBrands();
		Thread.sleep(2000);
		Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
				"04-MilkBrands");
		driver.navigate().back();
		Thread.sleep(2000);
		
	}
}
