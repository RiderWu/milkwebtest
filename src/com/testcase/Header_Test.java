package com.testcase;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.pageobject.Header;
import com.pageobject.SearchResultsPage;
import com.web.Common;

public class Header_Test {

	public static String startTime = null;
	public WebDriver driver = null;
	public static String domain = null;
	public static String browser = null;
	public static String market = null;
	public static String width = null;
	public static String height = null;
	private static String path = "";
	public Header header = null;

	@Parameters({ "environment" })
	@BeforeSuite
	public void beforeSuite(String environment) throws Exception {
		SimpleDateFormat datetime = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		startTime = datetime.format(new Date());
		domain = environment;
	}

	@AfterSuite
	public void afterSuite() throws Exception {
		// MailUtility.sendMail();
	}

	@Parameters({ "browser", "market", "width", "height" })
	@BeforeClass
	public void beforeClass(String browser, String market, String width, String height) throws Exception {
		Header_Test.browser = browser;
		Header_Test.market = market;
		Header_Test.width = width;
		Header_Test.height = height;
		driver = Common.openBrowser(browser, "about:blank", width, height);
		header = new Header(driver);
		
	}

	@AfterClass
	public void afterClass() throws Exception {
		driver.quit();
		
	}

	@BeforeMethod
	public void beforeMethod() throws Exception {

	}

	@AfterMethod
	public void afterMethod() throws Exception {
		
	}
	
	
	@Test
	public void test_logo() {
		driver.navigate().to(domain + path);
		Assert.assertTrue(header.logoExists());
	}
	
	@Test
	public void test_menubar() {
		driver.navigate().to(domain + path);
		Assert.assertTrue(header.menuItemExists("Milk Nutrition"));
		Assert.assertTrue(header.menuItemExists("Milk Recipes"));
		Assert.assertTrue(header.menuItemExists("About Milk Life"));
		Assert.assertTrue(header.menuItemExists("The Great American Milk Drive"));
	}
	
	@Test
	public void test_search() {
		driver.navigate().to(domain + path);
		header.search("test");
		
		SearchResultsPage searchResultsPage = new SearchResultsPage(driver);
		Assert.assertTrue(searchResultsPage.titleExists());
	}
	
	@Test
	public void test_socialMedia() {
		driver.navigate().to(domain + path);
		Assert.assertTrue(header.socialMediaExist());
		Assert.assertEquals(header.getsocialMediaLink("facebook"), "https://www.facebook.com/milk");
		Assert.assertEquals(header.getsocialMediaLink("twitter"), "https://twitter.com/MilkLife");
		Assert.assertEquals(header.getsocialMediaLink("pinterest"), "https://www.pinterest.com/milk");
		Assert.assertEquals(header.getsocialMediaLink("instagram"), "http://instagram.com/milklife");
	}
	
	@Test
	public void test_secondaryNavBar() {
		driver.navigate().to(domain + path);
		Assert.assertTrue(header.secondaryNavBarExists());
		Assert.assertEquals(header.getMom4KidsLink(), domain + "/activities-kids-recipes");
		Assert.assertEquals(header.getTeamUSALink(), domain + "/teamusa");
		Assert.assertEquals(header.getHighProteinMorningLink(), domain + "/high-protein-morning");
		
	}
	
	@Test
	public void test_dropdownMenu() throws Exception {
		driver.navigate().to(domain + path);
		Assert.assertTrue(header.isDropdownPresent());
		
		Assert.assertEquals(header.getLinksInDropdown(0), "https://milklife.com/fun-breakfast-ideas");
		Assert.assertEquals(header.getLinksInDropdown(1), "https://milklife.com/milk-nutrition-articles");
		Assert.assertEquals(header.getLinksInDropdown(2), "https://milklife.com/fun-with-milk");
	}
	
	@Test
	public void test_subPageRedirection() throws Exception {
		driver.navigate().to(domain + path);
		
		header.mouseOverDropDownMenu();
		header.goToSubPage(0); // Go to fun-breakfast-ideas page
		Thread.sleep(2000);
		Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
				"01-GoToSubPage");
		driver.navigate().back();
		Thread.sleep(2000);
		
		header.mouseOverDropDownMenu();
		header.goToSubPage(1);  // Go to milk-nutrition-articles page
		Thread.sleep(2000);
		Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
				"02-GoToSubPage");
		driver.navigate().back();
		Thread.sleep(2000);
		
		header.mouseOverDropDownMenu();
		header.goToSubPage(2);  // Go to fun-with-milk
		Thread.sleep(2000);
		Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
				"03-GoToSubPage");
		driver.navigate().back();
		
	}
	
	
}
