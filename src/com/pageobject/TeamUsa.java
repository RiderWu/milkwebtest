package com.pageobject;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

public class TeamUsa {
public WebDriver driver = null;

	public TeamUsa(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
//ToDo: Please add elements for TeamUsa page
	
	@FindBy(xpath="/html/body/div[4]/div/div[2]/div/h1")
	private WebElement textblock;
	public WebElement getTextBlock()
	{
	return textblock;
	}
	@FindBy(xpath="/html/body/div[4]/div/div[2]/div/a")
	private WebElement FindOut;
	public WebElement getFindOut()
	{
		return FindOut;
	}
	@FindBy(css="p.d-promo-title")
	private WebElement SeeHow;
	public WebElement getSeeHow()
	{
		return SeeHow;
	}
	@FindBy(xpath="//div[2]/div/div/div/div/a/div[2]/p[2]")
	private WebElement WatchThis;
	public WebElement getWatchThis()
	{
		return WatchThis;
	}
	@FindBy(xpath="//div[3]/div/div/div/div/a/div[2]/p[2]")
	private WebElement MilkMatters;
	public WebElement getMilkMatters()
	{
		return MilkMatters;
	}
	@FindBy(css = "#block-system-main > div > div > div > section > div > h2")
	
	private WebElement MoreFromTeamMlik;
	public WebElement getMoreFromTeamMlik()
	{
		return MoreFromTeamMlik;
	}
	@FindBy(id="edit-sort-by-count")
	private WebElement MostViewed;
	public WebElement getMostViewed()
	{
		return MostViewed;
	}
	@FindBy(id="edit-sort-by-created")
	private WebElement MostRecent;
	public WebElement getMostRecent()
	{
		return MostRecent;
	}
	@FindBys({
		@FindBy(className = "o-related-article__content"),
		@FindBy(tagName = "a")
		})
		private List<WebElement> Recipes;
		public List<WebElement> getRecipes()
		{
			return Recipes;
		}
	@FindBy(className= "pager-load-more")
	private WebElement Loadmore;
	public WebElement getLoadmore()
	{
		return Loadmore.findElement(By.tagName("a"));
	}
}

