package com.pageobject;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

public class MilkNutritionArticlesPage {
public WebDriver driver = null;
	
	public MilkNutritionArticlesPage(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(className = "c-button--primary-colour")
	private WebElement HeroButton;
	public WebElement getHeroButton()
	{
		return HeroButton;
	}
	@FindBy(xpath = "//a/div[2]")
	private WebElement TalkButton;
	public WebElement getTalkButton()
	{
		return TalkButton;
	}
	@FindBy(xpath = "//div[2]/div/div/div/div/a/div[2]")
	private WebElement WhyButton;
	public WebElement getWhyButton()
	{
		return WhyButton;
	}
	@FindBy(xpath = "//div[3]/div/div/div/div/a/div[2]")
	private WebElement WhatButton;
	public WebElement getWhatButton()
	{
		return WhatButton;
	}
	@FindBy(className = "block-title")
	private WebElement Nutrition;
	public WebElement getNutrition()
	{
		return Nutrition;
	}
	@FindBy(id = "edit-sort-by-created")
	private WebElement MostRecent;
	public WebElement getMostRecent()
	{
		return MostRecent;
	}
	@FindBy(id = "edit-sort-by-count")
	private WebElement MostViewed;
	public WebElement getMostViewed()
	{
		return MostViewed;
	}
	@FindBys({
	@FindBy(className = "o-related-article__content"),
	@FindBy(tagName = "a")
	})
	private List<WebElement> Recipes;
	public List<WebElement> getRecipes()
	{
		return Recipes;
	}
	@FindBy(className = "pager-load-more")
	private WebElement Loadmore;
	public WebElement getLoadmore()
	{
		return Loadmore.findElement(By.tagName("a"));
	}
	
	@FindBy(id = "edit-tid-3-button")
	private WebElement Filter;
	public WebElement getFilter()
	{
		return Filter;
	}
	
	@FindBys({
		@FindBy(id = "edit-tid-3-menu"),
		@FindBy(tagName = "a")
	})
	private List<WebElement> Items;
	public List<WebElement> getItems()
	{
		return Items;
	}
	
	
	//ToDo: Please add elements for MilkNutritionArticlesPage
}
