package com.pageobject;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

public class FunBreakfastIdeasPage {
public WebDriver driver = null;
	
	public FunBreakfastIdeasPage(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	//ToDo: Please add elements for FunBreakfastIdeasPage
	
	@FindBy(xpath = "html/body/div[4]/div/div[2]/div/a")
	private WebElement serveItUp;
	public WebElement getServeItUp()
	{
		return serveItUp;
	}
	
	@FindBy(xpath = "html/body/div[4]/div/div[2]/div")
	private WebElement textblock;
	public WebElement getTextBlock()
	{
		return textblock;
	}
	
	@FindBys({
		@FindBy(className = "o-triple-content__block"),
		@FindBy(tagName = "a")
	})
	private WebElement protein;
	public WebElement getProtein()
	{
		return protein;
	}
	
	@FindBy(xpath ="//div[2]/div/div/div/div/a/div[2]")
	private WebElement WhyBreakfast;
	public WebElement getWhyBreakfast()
	{
		return WhyBreakfast;
	}
	
	@FindBy(xpath ="//div[3]/div/div/div/div/a/div[2]/p[1]")
	private WebElement Tastethese;
	public WebElement getTastethese()
	{
		return Tastethese;
	}
	
	@FindBy(css ="#block-system-main > div > div > div > section > div > h2")
	private WebElement Breakfast;
	public WebElement getBreakfast()
	{
		return Breakfast;
	}
	
	@FindBy(css="#edit-sort-by-count")
	private WebElement Mostviewed;
	public WebElement getMostViewed()
	{
		return Mostviewed;
	}
	
	@FindBy(css ="#edit-sort-by-created")
	private WebElement MostRecent;
	public WebElement getMostRecent()
	{
		return MostRecent;
	}
	
	@FindBys({
		@FindBy(className = "o-related-article__content"),
		//@FindBy(tagName = "a")
	})
	private List<WebElement> recipes;
	public List<WebElement> getRecipes()
	{
		return recipes;
	}
	
	@FindBy(className = "pager-load-more")
	private WebElement loadmore;
	public WebElement getLoadmore()
	{
		return loadmore.findElement(By.tagName("a"));
	}
	
	@FindBy(id = "edit-tid-2-button")
	private WebElement AnyButton;
	public WebElement getAnybutton()
	{
		return AnyButton;
	}
	
	
	
	@FindBys({
		@FindBy(id = "edit-tid-2-menu"),
		@FindBy(tagName = "a")
	})
	private List<WebElement> Items;
	public List<WebElement> getItems()
	{
		return Items;
	}
	
}
