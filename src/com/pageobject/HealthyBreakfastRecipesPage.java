package com.pageobject;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

public class HealthyBreakfastRecipesPage {
public WebDriver driver = null;
	
	public HealthyBreakfastRecipesPage(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	//ToDo: Please add elements for HealthyBreakfastRecipesPage
	@FindBy(id = "hero-image")
	private WebElement HeroImage;
	public WebElement getHeroImage()
	{
		return HeroImage;
	}
	
	@FindBy(xpath = "//a[contains(text(),'Batter up')]")
	private WebElement BatterUp;
	public WebElement getBatterUp()
	{
		return BatterUp;
	}
	
	@FindBy(xpath = "//a/div[2]")
	private WebElement LetsTalkProtein;
	public WebElement getLetsTalkProtein()
	{
		return LetsTalkProtein;
	}
	
	@FindBy(xpath = "//p")
	private WebElement PrepareAPeanutButterPuppySandwich;
	public WebElement getPrepareAPeanutButterPuppySandwich()
	{
		return PrepareAPeanutButterPuppySandwich;
	}
	
	@FindBy(xpath = "//div[3]/div/div/div/div/a/div[2]")
	private WebElement BatterUpLink;
	public WebElement getBatterUpLink()
	{
		return BatterUpLink;
	}
	
	@FindBy(xpath = "//section/div/h2")
	private WebElement RecipesTitle;
	public WebElement getRecipesTitle()
	{
		return RecipesTitle;
	}
	
	@FindBy(xpath = "//div[@id='edit-sort-by-created']/a")
	private WebElement MostRecent;
	public WebElement getMostRecent()
	{
		return MostRecent;
	}
	
	@FindBy(xpath = "//div[@id='edit-sort-by-count']/a")
	private WebElement MostReviewed;
	public WebElement getMostReviewed()
	{
		return MostReviewed;
	}
	
	@FindBys({
		@FindBy(className = "o-related-article__content"),
		//@FindBy(tagName = "a")
	})
	private List<WebElement> Recipes;
	public List<WebElement> getRecipes()
	{
		return Recipes;
	}
	
	
	@FindBy(className = "pager-load-more")
	private WebElement Loadmore;
	public WebElement getLoadmore()
	{
		return Loadmore.findElement(By.tagName("a"));
	}
	
	@FindBys({
		@FindBy(id = "edit-tid-4-menu"),
		@FindBy(tagName = "a")
	})
	private List<WebElement> Items;
	public List<WebElement> getItems()
	{
		return Items;
	}
	
	@FindBy(xpath = "//a/span")
	private WebElement RecipesType;
	public WebElement getRecipesType()
	{
		return RecipesType;
	}
	
	@FindBy(xpath = "//ul[@id='edit-tid-4-menu']/li[2]/a")
	private WebElement BakedGoods;
	public WebElement getBakedGoods()
	{
		return BakedGoods;
	}
	
	@FindBy(xpath = "//ul[@id='edit-tid-4-menu']/li[3]/a")
	private WebElement FreshOfftheGriddle;
	public WebElement getFreshOfftheGriddle()
	{
		return FreshOfftheGriddle;
	}
	
	@FindBy(xpath = "//ul[@id='edit-tid-4-menu']/li[4]/a")
	private WebElement MakeAhead;
	public WebElement getMakeAhead()
	{
		return MakeAhead;
	}
	
	@FindBy(xpath = "//ul[@id='edit-tid-4-menu']/li[5]/a")
	private WebElement EggCellentEats;
	public WebElement getEggCellentEats()
	{
		return EggCellentEats;
	}
	
	@FindBy(xpath = "//ul[@id='edit-tid-4-menu']/li[6]/a")
	private WebElement Smoothies;
	public WebElement getSmoothies()
	{
		return Smoothies;
	}
	
	@FindBy(xpath = "//ul[@id='edit-tid-4-menu']/li[7]/a")
	private WebElement TeaCoffeeCocoa;
	public WebElement getTeaCoffeeCocoa()
	{
		return TeaCoffeeCocoa;
	}
	
	@FindBy(xpath = "//ul[@id='edit-tid-4-menu']/li[8]/a")
	private WebElement WarmCereals;
	public WebElement getWarmCereals()
	{
		return WarmCereals;
	}
	
	@FindBy(xpath = "//ul[@id='edit-tid-4-menu']/li[9]/a")
	private WebElement MyPlateBreakfasts;
	public WebElement getMyPlateBreakfasts()
	{
		return MyPlateBreakfasts;
	}
	
	@FindBy(xpath = "//ul[@id='edit-tid-4-menu']/li/a")
	private WebElement All;
	public WebElement getAll()
	{
		return All;
	}

	
	
}

