package com.pageobject;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MessagesAndHelp {
public WebDriver driver = null;
	
	public MessagesAndHelp(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(css = ".fixed-share-links a")
	private List<WebElement> shareLinksOnRightBar;
	
	@FindBy(css = "iframe[id *= '_grid']")
	private WebElement pinterestIFrame;
	
	@FindBy(id = "choosePin")
	private WebElement pinTerestTitleInIFrame;
	
	public boolean isRightBarPresented() {
		for(WebElement e : shareLinksOnRightBar) {
			if(!e.isDisplayed()) {
				return false;
			}
		}
		return true;
	}
	
	public void clickFacebookOnRightBar() {
		shareLinksOnRightBar.get(0).click();
	}
	
	public void clickTwitterOnRightBar() {
		shareLinksOnRightBar.get(1).click();
	}
	
	public void clickPinterestOnRightBar() {
		shareLinksOnRightBar.get(2).click();
	}
	
	public String getPinterestTitle() {
		return pinTerestTitleInIFrame.getText();
	}
	
	public WebElement getIFrame() {
		return pinterestIFrame;
	}
	
	public void waitForIFrameAppear() {
		new WebDriverWait(driver, 10, 100).until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(pinterestIFrame));
	}
	
}
