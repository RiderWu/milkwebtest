package com.pageobject;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

public class HighProteinMorningPage {

	public WebDriver driver = null;

	public HighProteinMorningPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	// header
	//*[@id="lets-talk-protein"]
	@FindBy(id = "lets-talk-protein")
	private WebElement topHeader;
	public WebElement getTopHeader() {
		return topHeader;
	}

	// chevron symbol
	//*[@id="protein-chevron"]/img
	//@FindBy(xpath="//a[@id='protein-chevron']/img")
	@FindBy(id="protein-chevron")
	private WebElement chevronSymbol;
	public WebElement getChevronSymbol() {
		return chevronSymbol;
	}

	// title above video
	// *[@id="protein-basics"]/div/div[1]/p
	@FindBy(id = "primary-menu-bar")
	private WebElement greyNavigation;
	public WebElement getGreyNavigation() {
		return greyNavigation;
	}
	
	@FindBy(xpath = "//div[@id='protein-basics']/div/div[1]/p")
	private WebElement proteinBasics;
	public WebElement getproteinBasics() {
		return proteinBasics;
	}

	// play button on the video
	// *[@id="video-open-btn"]/img
	@FindBy(xpath = "//a[@id='video-open-btn']/img")
	private WebElement videoPlay;
	public WebElement getVideoPlay() {
		return videoPlay;
	}

	// close button on the video
	// *[@id="video-close-btn"]/img
	@FindBy(xpath = "//a[@id='video-close-btn']/img")
	private WebElement videoClose;
	public WebElement getVideoClose() {
		return videoClose;
	}

	//copy above tab buttons1
	//*[@id="protein-basics"]/div/div[2]/div[1]/p[5]
	//div[@id='protein-basics']/div/div[2]/div/p[5] 
	//@FindBy(id = "what-is-protein")
	//*[@id="protein-basics"]/div/div[2]/div[1]/p[4]
	@FindBy(xpath = "//div[@id='protein-basics']/div/div[2]/div[1]/p[4]")
	private WebElement copy1;
	public WebElement getCopy1() {
		return copy1;
	}
	
	// tab buttons1
	@FindBys({ @FindBy(id = "protein-basics-right-accordian"), @FindBy(className = "tab-btn") })
	private List<WebElement> proteinTabItems1;
	public List<WebElement> getProteinTabItems1() {
		return proteinTabItems1;
	}

	// copy above tab buttons2
	//*[@id="protein-in-milk"]/div/div[2]/div[1]/p[3]
	@FindBy(xpath = "//div[@id='protein-in-milk']/div/div[2]/div[1]/p[3]")
	private WebElement copy2;
	public WebElement getCopy2() {
		return copy2;
	}
	
	// tab buttons2
	@FindBys({ @FindBy(id = "protein-in-milk-accordian"), @FindBy(className = "tab-btn") })
	private List<WebElement> proteinTabItems2;
	public List<WebElement> getProteinTabItems2() {
		return proteinTabItems2;
	}

	// copy above tab buttons3
	//*[@id="why-timing-matters"]/div/div[2]/div[1]/p[2]/br[2]
	//*[@id="why-timing-matters"]/div/div[2]/div[1]/p[2]
	//*[@id="why-timing-matters"]/div/div[2]/div[1]/p[1]
	@FindBy(xpath = "//div[@id='why-timing-matters']/div/div[2]/div[1]/p[1]")
	private WebElement copy3;
	public WebElement getCopy3() {
		return copy3;
	}

	// tab buttons3
	@FindBys({ @FindBy(id = "why-timing-matters-accordian"), @FindBy(className = "tab-btn") })
	private List<WebElement> proteinTabItems3;

	public List<WebElement> getProteinTabItems3() {
		return proteinTabItems3;
	}

	// learn more title
	// *[@id="protein-proteinMilk"]/div/p
	@FindBy(xpath = "//div[@id='protein-proteinMilk']/div/p")
	private WebElement learnMore;
	public WebElement getLearnMore() {
		return learnMore;
	}
	
	// learn more first circle
	@FindBy(id = "circle-btn-versatility")
	private WebElement proteinQuality;
	public WebElement getProteinQuality() {
		return proteinQuality;
	}

	// learn more second circle
	@FindBy(id = "circle-btn-cost")
	private WebElement nutrientValue;
	public WebElement getNutrientValue() {
		return nutrientValue;
	}

	// learn more third circle
	@FindBy(id = "circle-btn-value")
	private WebElement cost;
	public WebElement getCost() {
		return cost;
	}

	// learn more fourth circle
	@FindBy(id = "circle-btn-quality")
	private WebElement versatility;
	public WebElement getVersatility() {
		return versatility;
	}
	
	//title in protein intake section
	//*[@id="how-much-protein"]/div/div[1]/h2
	@FindBy(xpath="//div[@id='how-much-protein']/div/div[1]/h2")
	private WebElement intakeTitle;
	public WebElement getIntakeTitle() {
		return intakeTitle;
	}
	
	
	//handle button
	@FindBy(xpath="div[@id='js-rangeslider-0']/div[2]")
	private WebElement handleBtn;
	public WebElement getHandleBtn() {
		return handleBtn;
	}
	
	//age dropdown
	@FindBy(id="age-select-btn")
	private WebElement ageBtn;
	public WebElement getAgeBtn() {
		return ageBtn;
	}
	//5 years old
	//*[@id="dpi-holder"]/div[1]/div[3]/div[1]/ul/li[4]/a
	@FindBy(xpath="//div[@id='dpi-holder']/div[1]/div[3]/div[1]/ul/li[4]/a")
	private WebElement ageOption;
	public WebElement getAgeOption() {
		return ageOption;
	}
	//scroll bar
	//*[@id="js-rangeslider-1"]/div[1]
	@FindBy(xpath="//div[@id='js-rangeslider-1']/div[1]")
	private WebElement notActive;
	public WebElement getNotActive() {
		return notActive;
	}
	
	//copy above filter
	//*[@id="protein-tips-recipes"]/div/div[2]
	@FindBy(xpath="//div[@id='protein-tips-recipes']/div/div[2]")
	private WebElement leadingCopy;
	public WebElement getLeadingCopy() {
		return leadingCopy;
	}
	
	//filter all 
	@FindBy(id="filter-all")
	private WebElement filterAll;
	public WebElement getFilterAll() {
		return filterAll;
	}
	
	//reset button
	//*[@id="reset-filter"]/img
	@FindBy(xpath="//div[@id='reset-filter']/img")
	private WebElement resetBtn;
	public WebElement getResetBtn() {
		return resetBtn;
	}
	
	//filter one
	@FindBy(id="on-the-go")
	private WebElement oneTheGo;
	public WebElement getOneTheGo() {
		return oneTheGo;
	}

	// filter two
	@FindBy(id = "Smoothies")
	private WebElement smoothies;
	public WebElement getSmoothies() {
		return smoothies;
	}
	

	// filter three
	@FindBy(id = "kid-friendly")
	private WebElement kidFriendly;
	public WebElement getKidFriendly() {
		return kidFriendly;
	}
	
	//all filters
	@FindBy(id = "custom-filter")
	private WebElement allFilters;
	public WebElement getAllFilters() {
		return allFilters;
	}
	
	//select the first option
	//*[@id="protein-tips-recipes"]/div/div[4]/div/ul/li[1]/a[1]
	@FindBy(xpath="//div[@id='protein-tips-recipes']/div/div[4]/div/ul/li[1]/a[1]")
	private WebElement vegetarian;
	public WebElement getVegetarian() {
		return vegetarian;
	}
	// close all filters
	// *[@id="custom-filter-close"]/img  
	@FindBy(id = "custom-filter-close")
	private WebElement closeAllFilters;
	public WebElement getCloseAllFilters() {
		return closeAllFilters;
	}

	// Previous page
	@FindBy(id = "tip-recipe-btn-prev")
	private WebElement previousPage;
	public WebElement getPreviousPage() {
		return previousPage;
	}

	//next page
	@FindBy(id="tip-recipe-btn-next")
	private WebElement nextPage;
	public WebElement getNextPage() {
		return nextPage;
	}
	
	//first result module
	////div/div[5]/div[3]/div/div/div[2]/div
	////div[59]/div[2]/div
//	@FindBy(xpath="//div[59]/div[2]/div")
//	private WebElement firstModule;
//	public WebElement getFirstModule() {
//		return firstModule;
//	}
	//*[@id="tip-reciep-panel-close"]/img 
	@FindBys({ @FindBy(id= "protein-starter-gallery" ) , @FindBy(className = "recipe-tip-image"), @FindBy(className = "imgCover") })
	private List<WebElement> filterResults;
	public List<WebElement> getFilterResults() {
		return filterResults;
	}
	//*[@id="item-961"]/div[2]/div
	@FindBy(xpath="//div[@id='item-961']/div[2]/div")
	private WebElement firstModule;
	public WebElement getFirstModule() {
		return firstModule;
	}
	//*[@id="item-842"]/div[2]/div
	@FindBy(xpath="//div[@id='item-842']/div[2]/div")
	private WebElement secondModule;
	public WebElement getSecondModule() {
		return secondModule;
	}
	//*[@id="item-749"]/div[2]/div
	@FindBy(xpath="//div[@id='item-749']/div[2]/div")
	private WebElement thirdModule;
	public WebElement getThirdModule() {
		return thirdModule;
	}
	
	//*[@id="item-1726"]/div[2]/div
	@FindBy(xpath="//div[@id='item-1726']/div[2]/div")
	private WebElement anotherFirstModule;
	public WebElement getAnotherFirstModule() {
		return anotherFirstModule;
	}
	
	@FindBy(id="tip-reciep-panel-close")
	private WebElement aTag;
	public WebElement getATag() {
		return aTag;
	}
	
	
	//title above gallery
	@FindBy(id = "morning-protein-gallery")
	private WebElement copy;
	public WebElement getCopy() {
		return copy;
	}
	//child image
	@FindBy(id = "1837")
	private WebElement childImage;
	public WebElement getChildImage() {
		return childImage;
	}

	// fruit image
	@FindBy(id = "1855")
	private WebElement fruitImage;
	public WebElement getFruitImage() {
		return fruitImage;
	}

//	left button
	@FindBy(id = "gallery-btn-left")
	private WebElement leftBtn;
	public WebElement getLeftBtn() {
		return leftBtn;
	}

	@FindBy(id = "gallery-btn-right")
	private WebElement rightBtn;
	public WebElement getRightBtn() {
		return rightBtn;
	}
	
}
