package com.pageobject;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

public class GivePage {

	public WebDriver driver = null;

	public GivePage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(className = "max-w150")
	private WebElement giveHero;

	public WebElement getGiveHero() {
		return giveHero;
	}
	
	@FindBy(css = ".max-container > a")
	private WebElement giveNow;

	public WebElement getGiveNow() {
		return giveNow;
	}

	@FindBy(className = "btn-play")
	private WebElement watchVideo;

	public WebElement getWatchVideo() {
		return watchVideo;
	}

	@FindBy(className = "le42")
	private WebElement total;

	public WebElement getTotal() {
		return total;
	}

	@FindBy(css = "#map path")
	private List<WebElement> map;
	public List<WebElement> getMap() {
		return map;
	}
	
	@FindBy(className = "panel-left")
	private WebElement food;

	public WebElement getFood() {
		return food;
	}
	
	@FindBy(css = ".bg-s100>h2")
	private WebElement check;

	public WebElement getCheck() {
		return check;
	}
	@FindBy(css = "div.field-content > div.item-wrapper > a > span.item-thumb > span.btn-play")
	private WebElement check1;

	public WebElement getCheck1() {
		return check1;
	}
	@FindBy(xpath = "//div[2]/div[2]/div/div/a/span/img")
	private WebElement check2;

	public WebElement getCheck2() {
		return check2;
	}
	@FindBy(xpath = "//div[3]/div[2]/div/div/a/span/img")
	private WebElement check3;

	public WebElement getCheck3() {
		return check3;
	}
	@FindBy(css = ".le42 > span")
	private WebElement name;

	public WebElement getName() {
		return name;
	}
	/*@FindBy(css = ".name > ")
	private WebElement logoName;

	public WebElement getLogoName() {
		return logoName;
	}*/
}
