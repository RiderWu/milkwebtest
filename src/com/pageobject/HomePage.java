package com.pageobject;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

public class HomePage {
public WebDriver driver = null;
	
	public HomePage(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(className = "view-content")
	private WebElement hero;
	public WebElement getHero()
	{
		return hero;
	}
	
	@FindBy(css = "div.owl-prev")
	private WebElement prev;
	public WebElement getPrev()
	{
		return prev;
	}
	
	@FindBy(css = "div.owl-next")
	private WebElement next;
	public WebElement getNext()
	{
		return next;
	}
	
	@FindBy(xpath = "(//div[@id='dale-home-page-down-arrow'])")
	private WebElement firstHeroScrollForMore;
	public WebElement getFirstHeroScrollForMore()
	{
		return firstHeroScrollForMore;
	}
	
	@FindBy(xpath = "(//div[@id='dale-home-page-down-arrow'])[2]")
	private WebElement evenHeroScrollForMore;
	public WebElement getEvenHeroScrollForMore()
	{
		return evenHeroScrollForMore;
	}
	
	@FindBy(xpath = "(//div[@id='dale-home-page-down-arrow'])[3]")
	private WebElement lastHeroScrollForMore;
	public WebElement getLastHeroScrollForMore()
	{
		return lastHeroScrollForMore;
	}
	
	@FindBy(className = "view-row")
	private WebElement banner;
	public WebElement getBanner()
	{
		return banner;
	}
	
	@FindBy(xpath = "//span/a")
	private WebElement donateMilk;
	public WebElement getDonateMilk()
	{
		return donateMilk;
	}
	
	@FindBy(xpath = "//section/div/h2")
	private WebElement featuredContentTitle;
	public WebElement getFeaturedContentTitle()
	{
		return featuredContentTitle;
	}
	
	@FindBys({
		@FindBy(className = "views-field-field-item-more"),
		@FindBy(tagName = "a")
	})
	private List<WebElement> featuredContentItems;
	public List<WebElement> getFeaturedContentItems()
	{
		return featuredContentItems;
	}
	
	@FindBy(xpath = "//div[@id='home-page']/div/div/div[3]/div/div/div/div/div[1]")
	private WebElement viewsRowFirst;
	public WebElement getViewsRowFirst()
	{
		return viewsRowFirst;
	}
	
	public WebElement getViewsRowFirstButton()
	{
		return viewsRowFirst.findElement(By.className("views-field-field-item-more")).findElement(By.tagName("a"));
	}
	
	@FindBy(xpath = "//div[@id='home-page']/div/div/div[3]/div/div/div/div/div[2]")
	private WebElement viewsRowEven;
	public WebElement getViewsRowEven()
	{
		return viewsRowEven;
	}
	
	public WebElement getViewsRowEvenButton()
	{
		return viewsRowEven.findElement(By.className("views-field-field-item-more")).findElement(By.tagName("a"));
	}
	
	@FindBy(xpath = "//div[@id='home-page']/div/div/div[3]/div/div/div/div/div[3]")
	private WebElement viewsRowLast;
	public WebElement getViewsRowLast()
	{
		return viewsRowLast;
	}
	
	public WebElement getViewsRowLastButton()
	{
		return viewsRowLast.findElement(By.className("views-field-field-item-more")).findElement(By.tagName("a"));
	}
	
	@FindBy(xpath = "//div[@id='home-page']/div/div/section[3]/div/h2")
	private WebElement whatsHappening;
	public WebElement getWhatsHappening()
	{
		return whatsHappening;
	}
	
	@FindBy(id = "quicktabs-tab-whats_happening_quicktab-0")
	private WebElement mostRecent;
	public WebElement getMostRecent()
	{
		return mostRecent;
	}
	
	@FindBy(id = "quicktabs-tab-whats_happening_quicktab-1")
	private WebElement mostReviewed;
	public WebElement getMostReviewed()
	{
		return mostReviewed;
	}
	
	@FindBys({
		@FindBy(className = "o-related-article__content"),
		//@FindBy(tagName = "a")
	})
	private List<WebElement> recipes;
	public List<WebElement> getRecipes()
	{
		return recipes;
	}
	
	@FindBy(className = "pager-load-more")
	private WebElement loadmore;
	public WebElement getLoadmore()
	{
		return loadmore.findElement(By.tagName("a"));
	}
	
	@FindBy(xpath = "//section[4]/div/h2")
	private WebElement aboutMilkLifeTitle;
	public WebElement getAboutMilkLifeTitle()
	{
		return aboutMilkLifeTitle;
	}
	
	@FindBy(className = "c-button--primary-colour")
	private WebElement readmore;
	public WebElement getReadmore()
	{
		return readmore;
	}
	
	@FindBy(className = "sub-title")
	private WebElement myMorningProteinGalleryTitle;
	public WebElement getMyMorningProteinGalleryTitle()
	{
		return myMorningProteinGalleryTitle;
	}
	
	@FindBy(id = "gallery-btn-left")
	private WebElement myMorningProteinGalleryLeftButton;
	public WebElement getMyMorningProteinGalleryLeftButton()
	{
		return myMorningProteinGalleryLeftButton;
	}
	
	@FindBy(id = "gallery-btn-right")
	private WebElement myMorningProteinGalleryNextRightButton;
	public WebElement getMyMorningProteinGalleryRightButton()
	{
		return myMorningProteinGalleryNextRightButton;
	}
	
	@FindBy(className = "owl-stage")
	private WebElement myMorningProteinGallery;
	public WebElement getMyMorningProteinGallery()
	{
		return myMorningProteinGallery;
	}
	
	@FindBys({
		@FindBy(className = "owl-stage"),
		@FindBy(className = "active")
	})
	private List<WebElement> activeGalleryItems;
	public List<WebElement> getActiveGalleryItems()
	{
		return activeGalleryItems;
	}
}
