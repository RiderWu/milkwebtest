package com.pageobject;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

public class Mom4KidsPage {

public WebDriver driver = null;
	
	public Mom4KidsPage(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id = "hero-image")
	private WebElement heroImage;
	public WebElement getHeroImage() {
		return heroImage;
	}
	
	//CTA　button
	@FindBy(className = "c-button")
	private WebElement goNowBtn;
	public WebElement getGoNowBtn() {
		return goNowBtn;
	}
	
	//three links below the CTA button
	@FindBys(@FindBy(className="d-promo-title"))
	private List<WebElement> featureLinks;
	public List<WebElement> getFeatureLinks() {
		return featureLinks;
	}
	//title on the activities
	@FindBy(css="h2.pane-title.block-title")
	private WebElement activitiesTitle;
	public WebElement getActivitiesTitle() {
		return activitiesTitle;
	}
	
	// sort by recent button
	@FindBy(linkText="Most Recent")
	private WebElement mostRecentBtn;
	public WebElement getMostRecentBtn() {
		return mostRecentBtn;
	}
	@FindBy(linkText="Most Viewed")
	private WebElement mostViewedBtn;
	public WebElement getMostViewBtn() {
		return mostViewedBtn;
	}
	
	@FindBys(@FindBy(className="o-related-article__content"))
	private List<WebElement> articleLinks;
	public List<WebElement> getArticleLinks() {
		return articleLinks;
	}
	
	@FindBy(linkText="Load more")
	private WebElement loadMoreBtn;
	public WebElement getLoadMoreBtn() {
		return loadMoreBtn;
	}
	
	
}
