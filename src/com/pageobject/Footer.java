package com.pageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Footer {
	public WebDriver driver = null;

	public Footer(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(css = ".menu-item-619 a")
	private WebElement contactUsLink;

	@FindBy(css = ".menu-item-620 a")
	private WebElement privacyPolicyLink;

	@FindBy(css = ".menu-item-643 a")
	private WebElement ourPartnersLink;

	@FindBy(css = ".menu-item-634 a")
	private WebElement milkBrandsLink;

	@FindBy(css = ".menu-item-621 a")
	private WebElement spanishSiteLink;

	@FindBy(css = ".menu-item-741 a")
	private WebElement facebookIcon;

	@FindBy(css = ".menu-item-740 a")
	private WebElement twitterIcon;

	@FindBy(css = ".menu-item-742 a")
	private WebElement pinterestIcon;

	@FindBy(css = ".menu-item-744 a")
	private WebElement instagramIcon;

	@FindBy(css = ".menu-item-743 a")
	private WebElement youtubeIcon;

	@FindBy(linkText = "Also check out builtwithchocolatemilk.com")
	private WebElement externalSiteLink;

	// @FindBy()
	// private WebElement copyRight;

	public boolean isFooterMenuPresented() {
		return contactUsLink.isDisplayed() && privacyPolicyLink.isDisplayed() && ourPartnersLink.isDisplayed()
				&& milkBrandsLink.isDisplayed() && spanishSiteLink.isDisplayed();
	}

	public boolean isSocialMediaPresented() {
		return facebookIcon.isDisplayed() && twitterIcon.isDisplayed() && pinterestIcon.isDisplayed()
				&& instagramIcon.isDisplayed() && youtubeIcon.isDisplayed();
	}
	
	public void clickContactUs() {
		contactUsLink.click();
	}
	
	public void clickPrivacyPolicy() {
		privacyPolicyLink.click();
	}
	
	public void clickOurPartners() {
		ourPartnersLink.click();
	}
	
	public void clickMilkBrands() {
		milkBrandsLink.click();
	}
	
	public String getFacebookLink() {
		return facebookIcon.getAttribute("href");
	}
	
	public String getTwitterLink() {
		return twitterIcon.getAttribute("href");
	}
	
	public String getPinterestLink() {
		return pinterestIcon.getAttribute("href");
	}
	
	public String getInstagramLink() {
		return instagramIcon.getAttribute("href");
	}
	
	public String getYoutubeLink() {
		return youtubeIcon.getAttribute("href");
	}
	
	public String getSpanishSiteLink() {
		return spanishSiteLink.getAttribute("href");
	}
	
	public String getExternalSiteLink() {
		return externalSiteLink.getAttribute("href");
	}
}
