package com.pageobject;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

public class FunWithMilkPage {
public WebDriver driver = null;
	
	public FunWithMilkPage(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	//ToDo: Please add elements for FunWithMilkPage
	@FindBy(className = "full-width-hero-image")
	private WebElement funWithMilkHero;
	public WebElement getFunWithMilkHero()
	{
		return funWithMilkHero;
	}
	
	//CTA on the hero image
	//@FindBy(xpath="//div[2]/div/a")
	@FindBy(xpath="/html/body/div[4]/div/div[2]/div/a")
	private WebElement farmFresh;
	public WebElement getFarmFresh()
	{
		return farmFresh;
	}
	
//three links
	@FindBys({
		@FindBy(className = "o-triple-content__block"),
		@FindBy(tagName = "a")
	})
	private List<WebElement> featuredContentItems;
	public List<WebElement> getFeaturedContentItems()
	{
		return featuredContentItems;
	}

	@FindBy(xpath="//div[@id='block-system-main']/div/div/div/section/div/h2") 
	//*[@id="block-system-main"]/div/div/div/section/div/h2
	private WebElement funWithMilkTastyTips;
	public WebElement getFunWithMilkTastyTips()
	{
		return funWithMilkTastyTips;
	}
//Tasty Tips
//	@FindBy(xpath="(div[@id='edit-sort-by-created/a'])")   
	//*[@id="edit-sort-by-created"]/a
	@FindBy(id="edit-sort-by-created")   
	private WebElement funWithMilkMostRecent;
	public WebElement getFunWithMilkMostRecent()
	{
		return funWithMilkMostRecent;
	}
	
	@FindBy(id = "edit-sort-by-count")
	private WebElement funWithMilkMostViewed;
	public WebElement getFunWithMilkMostViewed()
	{
		return funWithMilkMostViewed;
	}
	
	
	@FindBy(id = "edit-tid-1-button")
	private WebElement funWithMilkStatus;
	public WebElement getFunWithMilkStatus()
	{
		return funWithMilkStatus;
	}
	
	@FindBy(id = "edit-tid-1-menu")
	private WebElement funWithMilkMenu;
	public WebElement getFunWithMilkMenu()
	{
		return funWithMilkMenu;
	}
	
	
	@FindBy(xpath="//ul[@id='edit-tid-1-menu']/li[1]/a")
	private WebElement funWithMilkOption1;
	public WebElement getFunWithMilkOption1()
	{
		return funWithMilkOption1;
	}
	
	//*[@id="edit-tid-1-menu"]/li[2]/a
//	@FindBy(xpath="//div[10]/ul/li[2]/a")
//	@FindBy(xpath="//a[contains(@href, '#nogo')])[2]")
	@FindBy(xpath="//ul[@id='edit-tid-1-menu']/li[2]/a")
	private WebElement funWithMilkOption2;
	public WebElement getFunWithMilkOption2()
	{
		return funWithMilkOption2;
	}
	//*[@id="edit-tid-1-menu"]/li[3]/a
	@FindBy(xpath="//ul[@id='edit-tid-1-menu']/li[3]/a")
	private WebElement funWithMilkOption3;
	public WebElement getFunWithMilkOption3()
	{
		return funWithMilkOption3;
	}
	
	@FindBy(xpath="//ul[@id='edit-tid-1-menu']/li[4]/a")
	private WebElement funWithMilkOption4;
	public WebElement getFunWithMilkOption4()
	{
		return funWithMilkOption4;
	}
	
	
	@FindBy(className = "o-related-article__content")
	private List<WebElement> recipes;
	public List<WebElement> getRecipes()
	{
		return recipes;
	}
	
	@FindBy(className = "pager-load-more")
	private WebElement loadmore;
	public WebElement getLoadmore()
	{
		return loadmore.findElement(By.tagName("a"));
	}
	
}
