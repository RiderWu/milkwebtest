package com.pageobject;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Header {
	public WebDriver driver = null;

	public Header(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "name-and-slogan")
	private WebElement logo;

	@FindBy(css = ".menu-navbar-item")
	private List<WebElement> menuNavBar;

	@FindBy(id = "edit-search-block-form--2")
	private WebElement searchBar;

	@FindBy(css = "a[title='Facebook']")
	private WebElement facebookIcon;

	@FindBy(css = "a[title='Twitter']")
	private WebElement twitterIcon;

	@FindBy(css = "a[title='Pinterest']")
	private WebElement pinterestIcon;

	@FindBy(css = "a[title='Instagram']")
	private WebElement instagramIcon;

	@FindBy(linkText = "FUELING TEAM USA")
	private WebElement teamUSALink;
	
	@FindBy(linkText = "FAMILY ZONE")
	private WebElement mom4KidsLink;


	@FindBy(linkText = "PROTEIN POWER UP")
	private WebElement highProteinMorningLink;

	@FindBy(linkText = "THE GREAT AMERICAN MILK DRIVE")
	private WebElement giveLink;
	
	@FindBy(css = ".dropdown-menu-navbar")
	private List<WebElement> dropdownMenu;
	
	private List<WebElement> dropdownItems = null;

	public boolean logoExists() {
		return logo.isDisplayed();
	}

	public boolean menuItemExists(String menuName) {
		for (WebElement element : menuNavBar) {
			if (element.isDisplayed() && element.getText().equals(menuName.toUpperCase())) {
				return true;
			}
		}
		return false;
	}

	public boolean facbookIconExists() {
		return facebookIcon.isDisplayed();
	}

	public boolean twitterIconExists() {
		return twitterIcon.isDisplayed();
		
	}

	public boolean pinterestIconExists() {
		return pinterestIcon.isDisplayed();
	}

	public boolean instagramIconExists() {
		return instagramIcon.isDisplayed();
	}

	public void search(String searchKey) {
		searchBar.click();
		searchBar.sendKeys(searchKey + "\n");
	}

	public boolean socialMediaExist() {
		return facebookIcon.isDisplayed() && twitterIcon.isDisplayed() && pinterestIcon.isDisplayed()
				&& instagramIcon.isDisplayed();
	}

	public String getsocialMediaLink(String socialMediaName) {
		if (socialMediaName.equalsIgnoreCase("facebook")) {
			return facebookIcon.getAttribute("href");
		} else if (socialMediaName.equalsIgnoreCase("twitter")) {
			return twitterIcon.getAttribute("href");
		} else if (socialMediaName.equalsIgnoreCase("pinterest")) {
			return pinterestIcon.getAttribute("href");
		} else if (socialMediaName.equalsIgnoreCase("instagram")) {
			return instagramIcon.getAttribute("href");
		} else {
			return null;
		}
	}

	public boolean secondaryNavBarExists() {
		return teamUSALink.isDisplayed() && highProteinMorningLink.isDisplayed() && giveLink.isDisplayed();
	}
	
	public String getTeamUSALink() {
		return teamUSALink.getAttribute("href");
	}
	
	public String getMom4KidsLink() {
		return mom4KidsLink.getAttribute("href");
	}
	public String getHighProteinMorningLink() {
		return highProteinMorningLink.getAttribute("href");
	}
	
	public String getGiveLink() {
		return giveLink.getAttribute("href");
	}
	
	public void mouseOverDropDownMenu() {
		mouseOver(menuNavBar.get(0));
	}
	
	private void mouseOver(WebElement element) {
		Actions action = new Actions(driver); 
		action.moveToElement(element).build().perform();
	}
	
	public boolean isDropdownPresent() {
		mouseOver(menuNavBar.get(0));
		return dropdownMenu.get(0).isDisplayed();
	}
	
	public String getLinksInDropdown(int index) {
		dropdownItems = dropdownMenu.get(0).findElements(By.cssSelector("li a"));
		return dropdownItems.get(index).getAttribute("href");
	}
	
	public void click(WebElement element) {
		element.click();
	}
	
	public void goToSubPage(int index) {
		dropdownItems = dropdownMenu.get(0).findElements(By.cssSelector("li a"));
		click(dropdownItems.get(index));
	}
}
