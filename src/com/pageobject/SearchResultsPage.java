package com.pageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SearchResultsPage {
	public WebDriver driver = null;

	public SearchResultsPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(css = "#block-system-main > h2:nth-child(2)")
	private WebElement pageTitle;

	public boolean titleExists() {
		return pageTitle.isDisplayed();
	}

}
